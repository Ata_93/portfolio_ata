﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources.Core;
using Windows.Globalization;
using Windows.Media.SpeechRecognition;
using Flickr.Annotations;
using Flickr.Infrastructure.Extensions;
using Flickr.Messages;
using GalaSoft.MvvmLight.Messaging;

namespace Flickr.Services.Voice
{
    public sealed class VoiceInputProcessor
    {
        private const double MinimumConfidence = 0.1;

        private readonly SpeechRecognizer _speechRecognizer = new SpeechRecognizer(DefaultLanguage);
        private static readonly Language DefaultLanguage = new Language("en-US");
        private readonly IMessenger _messenger;

        private ISpeechRecognitionConstraint _numbersConstraint;
        private ISpeechRecognitionConstraint _yearsConstraint;
        private SpeechRecognitionListConstraint _locationsConstraint;
        private readonly Dictionary<DictionaryType, ISpeechRecognitionConstraint> _wordsConstraints = new Dictionary<DictionaryType, ISpeechRecognitionConstraint>();

        private ISpeechRecognitionConstraint _pullInConstraint;
        private DictionaryType? _currentType;

        public VoiceInputProcessor([NotNull] IMessenger messenger)
        {
            if (messenger == null) throw new ArgumentNullException(nameof(messenger));
            _messenger = messenger;
        }

        public async Task InitializeVoiceInput(List<int> availableYears, IEnumerable<string> locations, int maxNumber)
        {
            IsInitialized = true;
            var yearsWords = availableYears.Select(y => y.ToWords()).ToList();
            _yearsConstraint = new SpeechRecognitionListConstraint(yearsWords);
            _locationsConstraint = new SpeechRecognitionListConstraint(locations);
            var numberWords = Enumerable.Range(1, maxNumber).Select(n => n.ToWords()).ToList();
            _numbersConstraint = new SpeechRecognitionListConstraint(numberWords);
            _pullInConstraint = CreateListOfConstraintsFromDictionaryConstraint(DictionaryType.PullIn);

            _speechRecognizer.Constraints.Add(_yearsConstraint);
            _speechRecognizer.Constraints.Add(_locationsConstraint);
            _speechRecognizer.Constraints.Add(_numbersConstraint);
            _speechRecognizer.Constraints.Add(_pullInConstraint);

            var values = Enum.GetValues(typeof(DictionaryType));
            foreach (var value in values)
            {
                var dictionaryType = (DictionaryType)value;
                if (dictionaryType == DictionaryType.PullIn)
                    continue;

                var constraints = CreateListOfConstraintsFromDictionaryConstraint(dictionaryType);
                _wordsConstraints[dictionaryType] = constraints;

                constraints.IsEnabled = false;
                _speechRecognizer.Constraints.Add(constraints);
            }

            var result = await _speechRecognizer.CompileConstraintsAsync();
            IsInitialized = result.Status == SpeechRecognitionResultStatus.Success;
        }

        public void ChangeDictionary(DictionaryType type)
        {
            var previousType = _currentType;
            if (previousType != null)
            {
                _wordsConstraints[previousType.Value].IsEnabled = false;
            }

            if (type == DictionaryType.Photostream)
            {
                _yearsConstraint.IsEnabled = true;
                _locationsConstraint.IsEnabled = true;
                _pullInConstraint.IsEnabled = true;
            }
            else if (type == DictionaryType.Album || type == DictionaryType.AlbumWithStory)
            {
                _pullInConstraint.IsEnabled = true;
                _locationsConstraint.IsEnabled = false;
                _yearsConstraint.IsEnabled = false;
            }
            else
            {
                _pullInConstraint.IsEnabled = false;
                _numbersConstraint.IsEnabled = false;
                _locationsConstraint.IsEnabled = false;
                _yearsConstraint.IsEnabled = false;
            }

            _currentType = type;
            DisableWordsConstraint();
            _wordsConstraints[type].IsEnabled = true;
        }

        private void DisableWordsConstraint()
        {
            var values = Enum.GetValues(typeof(DictionaryType));
            foreach (DictionaryType value in values)
            {
                if (_wordsConstraints.ContainsKey(value))
                    _wordsConstraints[value].IsEnabled = false;
            }
        }

        private static ISpeechRecognitionConstraint CreateListOfConstraintsFromDictionaryConstraint(DictionaryType type)
        {
            var speechResourceMap = ResourceManager.Current.MainResourceMap.GetSubtree(type.ToString());
            var speechContext = ResourceContext.GetForCurrentView();

            speechContext.Languages = new[] { DefaultLanguage.LanguageTag };
            var constraintList = speechResourceMap.Select(r => r.Value.Resolve(speechContext).ValueAsString).ToList();
            return new SpeechRecognitionListConstraint(constraintList);
        }

        public void AddYearsConstraint(List<int> availableYears)
        {
            var yearsWords = availableYears.Select(y => y.ToWords()).ToList();
            _yearsConstraint = new SpeechRecognitionListConstraint(yearsWords);
            _speechRecognizer.Constraints.Add(_yearsConstraint);
        }

        public void AddLocationsConstraint(IEnumerable<string> locations)
        {
            _locationsConstraint = new SpeechRecognitionListConstraint(locations);
            _speechRecognizer.Constraints.Add(_locationsConstraint);
        }

        public bool IsInitialized { get; set; }

        public async Task StartProcessingAsync()
        {
            if (!IsInitialized)
                return;

            if (_speechRecognizer.Constraints.Count == 0)
                throw new InvalidOperationException("Voice recognizer: No constraints");

            _speechRecognizer.ContinuousRecognitionSession.ResultGenerated += VoiceResultGenerated;
            _speechRecognizer.ContinuousRecognitionSession.Completed += VoiceRecognitionCompleted;

            await _speechRecognizer.ContinuousRecognitionSession.StartAsync();
        }

        private void VoiceResultGenerated(
            SpeechContinuousRecognitionSession sender,
            SpeechContinuousRecognitionResultGeneratedEventArgs args)
        {
            var command = args.Result.Text;
            var conf = Math.Round(args.Result.RawConfidence, 2);

            if (conf <= MinimumConfidence)
                return;

            _messenger.Send(new VoiceCommandMessage(command, conf));
        }

        private async void VoiceRecognitionCompleted(
            SpeechContinuousRecognitionSession sender,
            SpeechContinuousRecognitionCompletedEventArgs args)
        {
            try
            {
                if (args.Status != SpeechRecognitionResultStatus.UserCanceled)
                    await _speechRecognizer.ContinuousRecognitionSession.StartAsync();
            }
            catch (Exception)
            {
                //TODO: Unrecoverable voice exception. Send notification?
            }
        }

        public async Task StopProcessingAsync()
        {
            if (!IsInitialized)
                return;

            _speechRecognizer.ContinuousRecognitionSession.Completed -= VoiceRecognitionCompleted;
            if (_speechRecognizer.State != SpeechRecognizerState.Idle)
                await _speechRecognizer.ContinuousRecognitionSession.CancelAsync();
        }

        public void EnableNumbers()
        {
            _yearsConstraint.IsEnabled = false;
            _locationsConstraint.IsEnabled = false;
            if (_currentType != null)
                _wordsConstraints[_currentType.Value].IsEnabled = false;

            _numbersConstraint.IsEnabled = true;
        }

        public void DisableNumbers()
        {
            if (_currentType != null)
                _wordsConstraints[_currentType.Value].IsEnabled = true;

            _yearsConstraint.IsEnabled = true;
            _locationsConstraint.IsEnabled = true;
            _numbersConstraint.IsEnabled = false;
        }
    }
}