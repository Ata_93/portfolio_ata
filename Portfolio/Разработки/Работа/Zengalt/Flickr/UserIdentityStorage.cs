using System;
using System.Collections.Generic;
using Windows.Security.Credentials;

namespace Flickr.Infrastructure
{
    public class UserIdentityStorage
    {
        private const string FlickrUserIdResourceKey = "UserId";
        private const string FlickrTokensResourceKey = "Tokens";
        private const string UserIdResourcePassword = "pass";

        private readonly PasswordVault _passwordVault = new PasswordVault();
        private IReadOnlyList<PasswordCredential> _userIdResource;
        private IReadOnlyList<PasswordCredential> _tokensResource;

        public UserIdentityStorage()
        {
            try
            {
                _userIdResource = _passwordVault.FindAllByResource(FlickrUserIdResourceKey);
                _tokensResource = _passwordVault.FindAllByResource(FlickrTokensResourceKey);

                if (_userIdResource == null || _tokensResource == null)
                    return;

                UserId = _userIdResource[0].UserName;
                Token = _tokensResource[0].UserName;
                Secret = _passwordVault.Retrieve(FlickrTokensResourceKey, Token).Password;
            }
            catch (Exception)
            {
                UserId = Token = Secret = null;
            }
        }

        public string UserId { get; private set; }
        public string Token { get; private set; }
        public string Secret { get; private set; }

        public void SetUserIdentityService(string userId, string token, string secret)
        {
            if (string.IsNullOrEmpty(userId)) throw new ArgumentNullException(nameof(userId));
            if (string.IsNullOrEmpty(token)) throw new ArgumentNullException(nameof(token));
            if (string.IsNullOrEmpty(secret)) throw new ArgumentNullException(nameof(secret));

            UserId = userId;
            Token = token;
            Secret = secret;

            try
            {
                foreach (var credential in _tokensResource)
                    _passwordVault.Remove(credential);
                foreach (var credential in _userIdResource)
                    _passwordVault.Remove(credential);
            }
            catch (Exception ex)
            {
                //throw new ApplicatonException()
            }

            try
            {
                _passwordVault.Add(new PasswordCredential(FlickrUserIdResourceKey, userId, UserIdResourcePassword));
                _passwordVault.Add(new PasswordCredential(FlickrTokensResourceKey, token, secret));
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Couldn't store user credentials");
            }
        }

        public bool HasData()
        {
            return !string.IsNullOrEmpty(UserId) && !string.IsNullOrEmpty(Token) && !string.IsNullOrEmpty(Secret);
        }
    }
}