﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Media.Drm;
using NLog;

namespace PlayreadyLicenceServer.Services.Licence
{
    class LicenseAcknowledgementService
    {
        private static readonly Logger _loger = LogManager.GetCurrentClassLogger();

        private static readonly string LastTransactionID = LicenseAcquisitionService.LastTransactionID;

        private bool LookupTransaction(LicenseAcknowledgementChallenge challenge)
        {
            var transactionId = challenge.TransactionId;
            _loger.Log(LogLevel.Debug, challenge.ClientCertificate == null);

            var results = challenge.GetLicenseStorageResults(LicenseStorageResultsType.Successes);
            _loger.Log(LogLevel.Debug, "OLD " + LastTransactionID + "NEW " + transactionId);

            if ((string.Compare(LastTransactionID, transactionId, StringComparison.CurrentCulture) != 0) ||
                  results.Length == 0 ||
                  results[0].ErrorCode != 0)
            {
                _loger.Log(LogLevel.Debug, "FALSE");
                _loger.Log(LogLevel.Debug, "Result Length  " + results.Length);
                return false;
            }

            return true;
        }

        public LicenseAcknowledgementResponse HandleLicenseAcknowledgement(LicenseAcknowledgementChallenge challenge)
        {
            try
            {
                var response = new LicenseAcknowledgementResponse(challenge);
                _loger.Log(LogLevel.Debug, "STARTED AKNOWLEDGMENT!");

                if (!LookupTransaction(challenge))
                {
                    var sse = new ServiceSpecificException
                    {
                        CustomData = "Acknowledgement Not Verified."
                    };

                    if (challenge.Request != null)
                    {
                        var url = challenge.Request.Url.AbsoluteUri;
                        int lastSlash = url.LastIndexOf('/');
                        url = url.Substring(0, lastSlash) + "/error.htm";
                        sse.Url = new Uri(url);
                    }
                    _loger.Log(LogLevel.Debug, "Exception!");

                    throw sse;
                }

                _loger.Log(LogLevel.Debug, "Aknowledgment response returned!");
                return response;
            }
            catch (Exception e)
            {
                _loger.Log(LogLevel.Debug, e);
                return null;
            }
        }
    }
}
