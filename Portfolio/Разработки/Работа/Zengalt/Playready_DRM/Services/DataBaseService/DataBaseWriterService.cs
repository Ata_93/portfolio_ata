﻿using System;
using Microsoft.Media.Drm;
using NLog;
using PlayreadyLicenceServer.Parsers;
using PlayreadyLicenceServer.Services.DataBaseService.Models;

namespace PlayreadyLicenceServer.Services.DataBaseService
{
    public class DataBaseWriterService
    {
        private readonly LicenseChallengeParser _licenseChallengeParser = new LicenseChallengeParser();

        private readonly LicenseResponseParser _licenseResponseParser = new LicenseResponseParser();

        private static readonly Logger _loger = LogManager.GetCurrentClassLogger();

        public void WriteToDataBase(Guid currentSessionId, LicenseChallenge challenge, LicenseResponse licenseResponse = null, Exception e = null)
        {
            _loger.Log(LogLevel.Debug,"Starting Session");

            var sessionFactory = DataBaseSessionService.Instance.SessionFactory();

            using (var session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    
                    _loger.Log(LogLevel.Debug, "Transaction opened");
                    var request = new Request
                    {
                        ClientData = _licenseChallengeParser.ParseClientData(challenge.ClientInformation),
                        ClientTime = challenge.ClientTime,
                        ContentHeader = _licenseChallengeParser.ParseContentHeader(challenge.ContentHeader),
                        CustomData = challenge.CustomData,
                        HttpHeaders = _licenseChallengeParser.ParseHttpHeaders(challenge.Request.Headers),
                        Id = currentSessionId,
                        RequestTimeStamp = DateTime.Now,
                        SOAP = "Message"
                    };

                    var response = new Response
                    {
                        ExceptionMessage = e?.Message ?? "",
                        ExceptionStackTrace = e?.StackTrace ?? "",
                        HttpCode = e != null ? 500 : 200,
                        LicenseInformation = _licenseResponseParser.ParseLicenses(licenseResponse?.GetLicenses()) ?? "",
                        Id = currentSessionId,
                        TimeTaken = DateTime.Now
                    };
                    _loger.Log(LogLevel.Debug, "Start Saving");
                    session.Save(request);
                    session.Save(response);
                    _loger.Log(LogLevel.Debug, "Start commiting");
                    transaction.Commit();
                }
            }
        }
    }
}