﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace PlayreadyLicenceServer.Services.DataBaseService
{
    public class DataBaseSessionService
    {
        public static DataBaseSessionService Instance { get; } = new DataBaseSessionService();

        public ISessionFactory SessionFactory()
        {
            var model = CreateMappings();
            
            return Fluently.Configure()
            .Database(PostgreSQLConfiguration.PostgreSQL82
            .ConnectionString(c => c.Host("10.236.29.86")
                                    .Port(5432)
                                    .Database("pr")
                                    .Username("tim")
                                    .Password("123")))
                .ExposeConfiguration(BuildSchema)
                .Mappings(m => m
                .AutoMappings.Add(model))
                .BuildSessionFactory();
        }

        private void BuildSchema(Configuration config)
        {
            new SchemaExport(config).Create(false, true);
        }

        private AutoPersistenceModel CreateMappings()
        {
            return AutoMap.Assembly(System.Reflection.Assembly.GetCallingAssembly()).
                Where(t => t.Namespace == "PlayreadyLicenceServer.Services.DataBaseService.Models");
        }
    }
}
