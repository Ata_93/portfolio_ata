﻿using System;

namespace PlayreadyLicenceServer.Services.DataBaseService.Models
{
    public class Request
    {
        public virtual Guid Id { get; set; }

        public virtual DateTime RequestTimeStamp { get; set; }

        public virtual DateTime? ClientTime { get; set; }

        public virtual string ClientData { get; set; }

        public virtual string CustomData { get; set; }

        public virtual string ContentHeader { get; set; }

        public virtual string HttpHeaders { get; set; }

        public virtual string SOAP { get; set; }
    }
}