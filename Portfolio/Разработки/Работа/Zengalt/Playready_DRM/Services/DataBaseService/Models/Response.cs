﻿using System;

namespace PlayreadyLicenceServer.Services.DataBaseService.Models
{
    public class Response
    {
        public virtual Guid Id { get; set; }

        public virtual int HttpCode { get; set; }
        
        public virtual DateTime TimeTaken { get; set; }

        public virtual string ExceptionMessage { get; set; } = null;

        public virtual string ExceptionStackTrace { get; set; } = null;

        public virtual string LicenseInformation { get; set; }
    }
}
