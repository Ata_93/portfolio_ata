﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DataViewer.Common;

namespace DataViewer
{
    /// <summary>
    /// Класс, ответственный за создание/сохранение/загрузку проектов и хранение имени проекта.
    /// </summary>
    public class ProjectManager
    {

        #region - Constants -

        /// <summary>
        /// Фильтр для расширения файлов проекта программы.
        /// </summary>
        public const string ProjectFilter = "Data Viewer Project (*.dvproj)|*.dvproj";

        /// <summary>
        /// Расширение проекта
        /// </summary>
        public const string ProjectExtension = ".dvproj";

        /// <summary>
        /// Новое имя проекта.
        /// </summary>
        private const string NewProjectFilename = "\\New Project";

        #endregion - Constants - 

        //*********************************************************************************

        #region - Fields -  

        private string _projectFullName = Directory.GetCurrentDirectory() + NewProjectFilename + ProjectExtension;

        /// <summary>
        /// Показывает, есть ли в проекте несохраненные изменения.
        /// </summary>
        private bool _isProjectHaveUnsavedChanges;

        #endregion - Fields -

        //*********************************************************************************

        #region - Properties -

        /// <summary>
        /// Имя проекта по умолчанию
        /// </summary>
        public string DefaultProjectFilename 
        {
            get
            {
                return Directory.GetCurrentDirectory() + NewProjectFilename + ProjectExtension;
            }
        }

        /// <summary>
        /// Путь текущего проекта.
        /// </summary>
        public string ProjectFullName
        {
            get { return _projectFullName; }
            private set
            {
                if (!value.Contains(ProjectExtension))
                {
                    throw new NotImplementedException("Wrong work with Full Project File Names, please fix it!!!");
                }
                _projectFullName = value;
            }
        }

        /// <summary>
        /// Возвращает или устанавливает значение, есть ли в проекте несохраненные изменения.
        /// </summary>
        public bool IsProjectHaveUnsavedChanges
        {
            get { return _isProjectHaveUnsavedChanges; }
            set
            {
                _isProjectHaveUnsavedChanges = value;
                OnProjectModified();
            }
        }

        /// <summary>
        /// Возвращает значение, показывающее, был ли текущий проект сохранен хотя бы один раз.
        /// </summary>
        /// <remarks>Используется для определения, нужен Save или Save As.</remarks>
        public bool IsProjectSavedEver { get; private set; }

        #endregion - Properties -

        //*********************************************************************************

        #region - Events -

        /// <summary>
        /// Событие загорается в случаях: 1) когда в проекте происходят несохраненные изменения (добавление файлов).
        /// 2) когда проект сохраняется. 3) загружен новый проект 4) Создан новый проект.
        /// </summary>
        public event EventHandler ProjectModified;

        #endregion - Events -

        //*********************************************************************************

        #region - Private Methods - 

        /// <summary>
        /// Получить относительный путь из абсолютного относительно текущей директории.
        /// </summary>
        /// <param name="referencePath"></param>
        /// <returns></returns>
        private string GetRelativePath(string referencePath)
        {
            var activeDirectory =
                Path.GetFullPath(Directory.GetCurrentDirectory())
                    .Trim(Path.DirectorySeparatorChar)
                    .Split(Path.DirectorySeparatorChar);
            var relativeDirectory = referencePath.Split(Path.DirectorySeparatorChar);

            var directoryCount = 0;
            // расчет совпадающих папок
            while ((directoryCount < activeDirectory.Length) && (directoryCount < relativeDirectory.Length) &&
                   activeDirectory[directoryCount].Equals(relativeDirectory[directoryCount],
                       StringComparison.InvariantCultureIgnoreCase))
            {
                directoryCount++;
            }
            // если файл проекта и сохраняемые файлы находятся на разных дисках, оставляем абсолютный путь
            if (directoryCount == 0)
            {
                return referencePath;
            }

            var relativePath = new StringBuilder();
            // прибавление к пути переходов на уровень выше
            for (var i = directoryCount; i < activeDirectory.Length; i++)
            {
                relativePath.Append(".." + Path.DirectorySeparatorChar);
            }
            // прибавление к пути папок полного пути
            for (var i = directoryCount; i < relativeDirectory.Length; i++)
            {
                relativePath.Append(relativeDirectory[i] + Path.DirectorySeparatorChar);
            }
            // удаление последнего слеша
            relativePath.Length--;

            return relativePath.ToString();
        }

        private void OnProjectModified()
        {
            if (ProjectModified != null)
                ProjectModified(null, EventArgs.Empty);
        }
        
        #endregion - Private Methods -

        //*********************************************************************************

        #region - Public Methods - 

        /// <summary>
        /// Создать новый проект.
        /// </summary>
        public void NewProject()
        {
            ProjectFullName = DefaultProjectFilename;
            IsProjectHaveUnsavedChanges = false;
            IsProjectSavedEver = false;
        }

        /// <summary>
        /// Сохранить проект.
        /// </summary>
        /// <param name="projectFullName"></param>
        /// <param name="files"></param>
        public void SaveProject(string projectFullName, Dictionary<string, Dictionary<Filename, bool>> files)
        {
            var writer = new StreamWriter(projectFullName);
            foreach (var key in files.Keys)
            {
                var dictionary = new Dictionary<Filename, bool>(files[key]);
                writer.WriteLine(key);
                writer.WriteLine(dictionary.Count);
                foreach (var filenames in dictionary.Keys)
                {
                    writer.WriteLine(GetRelativePath(filenames.FullName));
                }
                foreach (var status in dictionary.Values)
                    writer.WriteLine(status);
            }
            writer.Close();
            ProjectFullName = projectFullName;
            IsProjectHaveUnsavedChanges = false;
            IsProjectSavedEver = true;
        }

        /// <summary>
        /// Загрузить проект.
        /// </summary>
        /// <param name="projectFullName"></param>
        /// <returns></returns>
        public Dictionary<string, Dictionary<Filename, bool>> OpenProject(string projectFullName)
        {
            //TODO: здесь нужно ловить все возможные системные исключения, а наверх выбрасывать специализированный ПрожектКорраптедЭксепшн
            var reader = new StreamReader(projectFullName);
            var viewerFiles = new Dictionary<string, Dictionary<Filename, bool>>();
            while (!reader.EndOfStream)
            {
                var buffer = reader.ReadLine();
                string name = null;
                if (buffer != null && buffer[0] == '$')
                    name = buffer.Remove(0, 1);
                int count = Convert.ToInt32(reader.ReadLine());
                var listOfKeys = new List<Filename>();
                var listOfValues = new List<bool>();
                for (int i = 0; i < count; i++)
                {
                    listOfKeys.Add(
                        new Filename(
                            Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), reader.ReadLine()))));
                }
                for (int i = 0; i < count; i++)
                {
                    listOfValues.Add(Convert.ToBoolean(reader.ReadLine()));
                }
                var dictionary = new Dictionary<Filename, bool>();
                for (int i = 0; i < count; i++)
                {
                    dictionary.Add(listOfKeys[i], listOfValues[i]);
                }
                viewerFiles.Add(name, dictionary);
            }
            reader.Close();
            ProjectFullName = projectFullName;
            IsProjectHaveUnsavedChanges = false;
            IsProjectSavedEver = true;
            return viewerFiles;
        }

        #endregion

    }
}
