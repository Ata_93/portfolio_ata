﻿namespace Indesys.Plugins.MeasurementSuite.ExtractionLinear.FieldTransistor
{
	//TODO: Пример реализации методики
	/// <summary>
    /// Класс, содержащий рассчётные формулы для методики экстракции модели
    /// по методу Dambrine
    /// </summary>
    public class DambrineFieldExtractionMethod : FieldTransistorExtractionMethodBaseClass
    {
        #region -Private Fields-

        /// <summary>
        /// Значение тока для первого файла
        /// </summary>
        private double Igs1 { get; set; }

        /// <summary>
        /// Значение тока для вторго файла
        /// </summary>
        private double Igs2 { get; set; }

        /// <summary>
        /// Матрицы параметров рассеяния для расчета внешних паразитных 
        /// сопротивлений и индуктивностей при Vds > 0 В и заданом токе Igs1.
        /// </summary>
        private ComplexMatrix2x2[] _coldModeMatricesIgs1;

        /// <summary>
        /// Матрицы параметров рассеяния для расчета внешних паразитных 
        /// сопротивлений и индуктивностей при Vds > 0 В и заданом токе Igs2.
        /// </summary>
        private ComplexMatrix2x2[] _coldModeMatricesIgs2;

        /// <summary>
        /// Матрицы параметров рассеяния для рассчёта внешних паразитных ёмкостей
        /// </summary>
        private ComplexMatrix2x2[] _coldModeMatricesVgsEqualVt;

        private Dictionary<string, Filename> _coldModeMatrixes = new Dictionary<string, Filename>
        {
            {"Cold Mode (Vds > 0, Igs1)", null}, 
            {"Cold Mode (Vds > 0, Igs2)", null},
            {"Cold Mode Equal", null}
        };

		#endregion 
        
        #region - Public properties -

        /// <summary>
        /// Изображение принципа работы того или иного метода.
        /// </summary>
        public override Image MethodImage
        {
            get
            {
                return new Bitmap(Properties.Resources.Dambrine);
            }
        }

        /// <summary>
        /// Панель добавления файлов.
        /// </summary>
        public override XtraUserControl AddFilesPanel
        {
            get { return _addFilesPanel; }
            set { _addFilesPanel = (DambrineAddFilesWindowControl)value; }
        }

        /// <summary>
        /// Внешние (паразитные) элементы схемы.
        /// </summary>
        public override Dictionary<string, PhysicalQuantityType> ExtrinsicElements
        {
            get
            {
                return new Dictionary<string, PhysicalQuantityType>
                                    {
                                       {"Rg", PhysicalQuantityType.Resistance},    
                                       {"Rd", PhysicalQuantityType.Resistance},
                                       {"Rs", PhysicalQuantityType.Resistance},    
                                       {"Lg", PhysicalQuantityType.Inductance},
                                       {"Ld", PhysicalQuantityType.Inductance},    
                                       {"Ls", PhysicalQuantityType.Inductance},
                                       {"Cpg", PhysicalQuantityType.Capacitance},
                                       {"Cpd", PhysicalQuantityType.Capacitance}
                                    };
            }
        }

        public override string MethodName
        {
            get { return "Dambrine"; }
        }

        /// <summary>
        /// Матрицы параметров рассеяния для расчета внешних параметров.
        /// </summary>
        public override Dictionary<string, Filename> ColdModeMatrixes
        {
            get { return _coldModeMatrixes; }
            set
            {
                _coldModeMatrixes = value;
                CalculateMatrix();
            }
        }

        /// <summary>
        /// Рассчитать внешние (паразитные) элементы схемы на указанной частоте.
        /// </summary>
        /// <param name="freq">Частота расчета.</param>
        /// <returns>Внешние параметры.</returns>
        public override List<double> GetExtrinsicParameters(double freq)
        {
            //CalculateMatrix();
            var index = FindFrequencyIndex(freq);
            
            var zParametersIg1 = ParameterConverter.Convert2x2(_coldModeMatricesIgs1[index], SignalMatrixTypeFlag.S, SignalMatrixTypeFlag.Z);
            var zParametersIg2 = ParameterConverter.Convert2x2(_coldModeMatricesIgs2[index], SignalMatrixTypeFlag.S, SignalMatrixTypeFlag.Z);
            var yParametersVgsEqualVt = ParameterConverter.Convert2x2(_coldModeMatricesVgsEqualVt[index], SignalMatrixTypeFlag.S, SignalMatrixTypeFlag.Y);
            var zParametersVgsEqualVt = ParameterConverter.Convert2x2(_coldModeMatricesVgsEqualVt[index], SignalMatrixTypeFlag.S, SignalMatrixTypeFlag.Z);

            var rc = zParametersIg2._11.Re - zParametersVgsEqualVt._11.Re;
            var rs = zParametersIg2._10.Re - rc / 2;
            // Значение Re(Z11) при условии бесконечности Igs (со слов Коколова)
            var reZ11OnInfIgs = zParametersIg2._00.Re - 
                (((zParametersIg2._00.Re - zParametersIg1._00.Re) / ((1 / Igs2) - (1 / Igs1))) *
                (1 / Igs2));
            var rg = reZ11OnInfIgs - rs - rc / 3;
            var rd = zParametersIg2._11.Re - rs - rc;

            var wf = 2.0 * Math.PI *Frequencies[index];
            var ls = zParametersIg1._01.Im / wf;
            var lg = (zParametersIg1._00.Im / wf) - ls;
            var ld = (zParametersIg1._11.Im / wf) - ls;

            var cb = -yParametersVgsEqualVt._01.Im / wf;
            var crg = (yParametersVgsEqualVt._00.Im / wf) - 2 * cb;
            var cpd = (yParametersVgsEqualVt._11.Im / wf) - cb;

            return new List<double> { rg, rd, rs, lg, ld, ls, crg, cpd };
        }

		/// <summary>
        /// Рассчитать параметры рассеяния модели для заданной частоты
        /// </summary>
        /// <param name="extrinsic">Внешние параметры модели</param>
        /// <param name="intrinsic">Внутренние пара</param>
        /// <param name="freq"></param>
        /// <returns></returns>
        public override ComplexMatrix2x2 GetSParameters(List<double> extrinsic, List<double> intrinsic, double freq)
        {
            
            var rg = extrinsic[0];
            var rd = extrinsic[1];
            var rs = extrinsic[2];
            var lg = extrinsic[3];
            var ld = extrinsic[4];
            var ls = extrinsic[5];
            var cpg = extrinsic[6];
            var cpd = extrinsic[7];

            var cgd = intrinsic[0];
            var cgs = intrinsic[1];
            var cds = intrinsic[2];
            var rds = intrinsic[3];
            var ri = intrinsic[4];
            var gm = intrinsic[5];
            var t = intrinsic[6];


            
            var wf = 2.0*Math.PI*freq;
            var wfToSquare = wf*wf;
            var cgsToSquare = cgs * cgs;
            var riToSquare = ri * ri;
            var D = 1 + wfToSquare * cgsToSquare * riToSquare;
            
            // Формулы взяты у Коколова
            // Учитываем внутренние параметры, рассчёт общий для Jeon, LICS, Dambrine
            var yParameters_intr = new ComplexMatrix2x2(
                new Complex((ri * cgsToSquare * wfToSquare) / D,
                    wf * (cgs / D + cgd)),
                new Complex(0, -wf * cgd),
                new Complex((gm * Math.Cos(wf * t)) / D - 
                    (gm * Math.Sin(wf * t) * wf * ri * cgs) / D,
                    -((gm * Math.Sin(wf * t)) / D) - 
                    (gm * Math.Cos(wf * t) * wf * ri * cgs) / D - wf * cgd),
                new Complex(1 / rds, wf * (cds + cgd))
                );
            var zParameters_intr = ParameterConverter.Convert2x2(yParameters_intr, SignalMatrixTypeFlag.Y,
                                                                 SignalMatrixTypeFlag.Z);
            // Учитываем внешние параметры
            zParameters_intr = zParameters_intr + new ComplexMatrix2x2(
                new Complex(rg + rs, wf * ls),
                new Complex(rs, wf * ls),
                new Complex(rs, wf * ls),
                new Complex(rd + rs, wf * ls)
                );
            var yParameters_intr2 = ParameterConverter.Convert2x2(zParameters_intr, SignalMatrixTypeFlag.Z,
                                                                  SignalMatrixTypeFlag.Y);
            yParameters_intr2 = yParameters_intr2 + new ComplexMatrix2x2(
                new Complex(0, wf * cpg),
                new Complex(0, 0),
                new Complex(0, 0),
                new Complex(0, wf * cpd)
                );
            var zParameters_intr2 = ParameterConverter.Convert2x2(yParameters_intr2, SignalMatrixTypeFlag.Y,
                                                                  SignalMatrixTypeFlag.Z);
            zParameters_intr2 = zParameters_intr2 + new ComplexMatrix2x2(
                new Complex(0, wf * lg),
                new Complex(0, 0),
                new Complex(0, 0),
                new Complex(0, wf * ld)
                );

            return ParameterConverter.Convert2x2(zParameters_intr2, SignalMatrixTypeFlag.Z, SignalMatrixTypeFlag.S);
        }

        #endregion
        
        #region - Private Methods -

        /// <summary>
        /// Метод рассчитывает Z параметры модели, без внешних элементов, 
        /// необходимо, чтобы рассчитать внутренние параметры модели
        /// </summary>
        /// <param name="sParameters">S параметры загруженные из файла в определённой рабочей точке</param>
        /// <param name="extrinsic">Рассчитанные внешние параметры модели</param>
        /// <param name="freq">Частота рассчёта</param>
        /// <returns>Z параметры модели без внешних элементов</returns>
        protected override ComplexMatrix2x2 SubstractExtrinsicParameters(ComplexMatrix2x2 sParameters,
            List<double> extrinsic, double freq)
        {
            // Формулы взял у Игоря, подробно описаны в методике Tairani
            var wf = 2.0*Math.PI*freq;
            var zParameters = ParameterConverter.Convert2x2(sParameters,
                SignalMatrixTypeFlag.S,
                SignalMatrixTypeFlag.Z);
            var zParameters_de = new ComplexMatrix2x2(
                new Complex(zParameters._00.Re, zParameters._00.Im - wf*extrinsic[3]),
                new Complex(zParameters._01.Re, zParameters._01.Im),
                new Complex(zParameters._10.Re, zParameters._10.Im),
                new Complex(zParameters._11.Re, zParameters._11.Im - wf*extrinsic[4])
                );
            var yParameters_de = ParameterConverter.Convert2x2(zParameters_de,
                SignalMatrixTypeFlag.Z,
                SignalMatrixTypeFlag.Y);
            var yParameters_de2 = new ComplexMatrix2x2(
                new Complex(yParameters_de._00.Re, yParameters_de._00.Im - wf*extrinsic[6]),
                new Complex(yParameters_de._01.Re, yParameters_de._01.Im),
                new Complex(yParameters_de._10.Re, yParameters_de._10.Im),
                new Complex(yParameters_de._11.Re, yParameters_de._11.Im - wf*extrinsic[7])
                );
            var zParameters_de2 = ParameterConverter.Convert2x2(yParameters_de2,
                SignalMatrixTypeFlag.Y,
                SignalMatrixTypeFlag.Z);
            var zParameters_de3 = new ComplexMatrix2x2(
                new Complex(zParameters_de2._00.Re - extrinsic[2] - extrinsic[0],
                    zParameters_de2._00.Im - wf*extrinsic[5]),
                new Complex(zParameters_de2._01.Re - extrinsic[2],
                    zParameters_de2._01.Im - wf*extrinsic[5]),
                new Complex(zParameters_de2._10.Re - extrinsic[2],
                    zParameters_de2._10.Im - wf*extrinsic[5]),
                new Complex(zParameters_de2._11.Re - extrinsic[2] - extrinsic[1],
                    zParameters_de2._11.Im - wf*extrinsic[5])
                );
            return ParameterConverter.Convert2x2(zParameters_de3, SignalMatrixTypeFlag.Z, SignalMatrixTypeFlag.Y);
        }

        /// <summary>
        /// Расчет матриц в холодном режиме.
        /// </summary>
        protected override void CalculateMatrix()
        {
            var coldParam1 = new XnPStorage();
            var coldParam2 = new XnPStorage();
            var coldParamEqual = new XnPStorage();

            //_coldModeMatrixes = _addFilesPanel.GetColdModeMatrixes();
            Igs1 = UnitTools.ConvertToSI(ProgramOptions.Instance.Current.Key, _addFilesPanel.Igs1);
            Igs2 = UnitTools.ConvertToSI(ProgramOptions.Instance.Current.Key, _addFilesPanel.Igs2);

            coldParam1.LoadFromFile(_coldModeMatrixes["Cold Mode (Vds > 0, Igs1)"].FullName);
            coldParam2.LoadFromFile(_coldModeMatrixes["Cold Mode (Vds > 0, Igs2)"].FullName);
            coldParamEqual.LoadFromFile(_coldModeMatrixes["Cold Mode Equal"].FullName);

            _coldModeMatricesIgs1 = coldParam1.GetSignalMatrices2x2().ToArray();
            _coldModeMatricesIgs2 = coldParam2.GetSignalMatrices2x2().ToArray();
            _coldModeMatricesVgsEqualVt = coldParamEqual.GetSignalMatrices2x2().ToArray();
            Frequencies.Sort();
        }

        #endregion
    }
}
