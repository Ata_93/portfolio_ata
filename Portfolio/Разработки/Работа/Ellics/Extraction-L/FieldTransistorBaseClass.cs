﻿namespace Indesys.Plugins.MeasurementSuite.ExtractionLinear.FieldTransistor
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;
	using Indesys.Plugins.MeasurementSuite.ExtractionLinear.Common;
    using Indesys.SDK.Common.Unit;
    using Indesys.SDK.DataStorage.DataStorages;
    using Indesys.SDK.Math.Matrix;
    
    /// <summary>
    /// Абстрактный класс, содержащий методы и свойства (общие и виртуальные) для методик экстракции.
    /// </summary>
    public abstract class FieldTransistorExtractionMethodBaseClass : IExportable
    {
        #region - Public Properties -
        
        /// <summary>
        /// Внешние (паразитные) элементы схемы.
        /// </summary>
        public abstract Dictionary<string, PhysicalQuantityType> ExtrinsicElements { get; }

        /// <summary>
        /// Название методики.
        /// </summary>
        public abstract string MethodName { get; }

        /// <summary>
        /// Внутренние элементы схемы.
        /// </summary>
        public Dictionary<string, PhysicalQuantityType> IntrinsicElements
        {
            get
            {
                return new Dictionary<string, PhysicalQuantityType>
                {
                    {"Cgd", PhysicalQuantityType.Capacitance},
                    {"Cgs", PhysicalQuantityType.Capacitance},
                    {"Cds", PhysicalQuantityType.Capacitance},
                    {"Rds", PhysicalQuantityType.Resistance},
                    {"Ri", PhysicalQuantityType.Resistance},
                    {"gm", PhysicalQuantityType.Conductance},
                    {"t", PhysicalQuantityType.Time}
                };
            }
        }

        /// <summary>
        /// Матрицы параметров рассеяния для расчета внешних параметров.
        /// </summary>
        public abstract Dictionary<string, Filename> ColdModeMatrixes { get; set; } 

        /// <summary>
        /// Список частот для расчета внешних параметров.
        /// </summary>
        public List<double> Frequencies { get; set; }

        /// <summary>
        /// Изображение принципа работы того или иного метода.
        /// </summary>
        public abstract Image MethodImage { get; }

		#endregion
        
		#region - Protected Methods -

        /// <summary>
        /// Расчет матриц в холодном режиме.
        /// </summary>
        protected abstract void CalculateMatrix();

        protected List<double> CalculateIntrinsicParameters(ComplexMatrix2x2 yParameters, double freq)
        {
            //var yParameters = SubstractExtrinsicParameters(sParameters, extrinsicParameters, freq);
            var y11 = yParameters._00;
            var y12 = yParameters._01;
            var y21 = yParameters._10;
            var y22 = yParameters._11;
            //Частота (омега)
            var wf = 2.0*Math.PI*freq;

            var cgd = -y12.Im/wf;
            var cgdAddition = y11.Im - wf*cgd;
            var cgs = cgdAddition*(1.0 + (y11.Re*y11.Re)/(cgdAddition*cgdAddition))/wf;
            var cds = (y22.Im - wf*cgd)/wf;
            var rds = 1.0/y22.Re;
            var ri = y11.Re/(cgdAddition*cgdAddition + y11.Re*y11.Re);

            var gm = Math.Sqrt((y21.Re*y21.Re + (y21.Im + wf*cgd)*(y21.Im + wf*cgd))*(1.0 + wf*cgs*ri*wf*cgs*ri));
            var t = 1.0/wf*Math.Asin((-wf*cgd - y21.Im - wf*cgs*ri*y21.Re)/gm);

            //return new List<double> { rgs, cgd, cgs, gm, t, rds, cds };
            return new List<double> {cgd, cgs, cds, rds, ri, gm, t};
        }

        protected abstract ComplexMatrix2x2 SubstractExtrinsicParameters(ComplexMatrix2x2 sParameters,
            List<double> extrinsic, double freq);

        public virtual void SetSchematicElements(ref ISchematic schematic, List<double> extrinsic,
            List<double> intrinsic) {}
        
        #endregion
        
        #region - Public Methods -

        /// <summary>
        /// Определяет индекс частоты в списке частот.
        /// </summary>
        /// <param name="frequency">Искомая частота.</param>
        /// <returns>Индекс искомой частоты.</returns>
        public int FindFrequencyIndex(double frequency)
        {
            if (frequency <= Frequencies[0])
            {
                return 0;
            }
            if (frequency > Frequencies[Frequencies.Count - 1])
            {
                return Frequencies.Count - 1;
            }

            for (var i = 1; i < Frequencies.Count; i++)
            {
                if (Frequencies[i] >= frequency)
                {
                    if ((Frequencies[i] - frequency) > (frequency - Frequencies[i - 1]))
                    {
                        return i - 1;
                    }
                    return i;
                }
            }
            return -1;
        }
        
        /// <summary>
        /// Рассчитать внешние (паразитные) элементы схемы на указанной частоте.
        /// </summary>
        /// <param name="freq">Частота расчета.</param>
        /// <returns>Внешние параметры.</returns>
        public abstract List<double> GetExtrinsicParameters(double freq);

        /// <summary>
        /// Рассчитать внутренние элементы схемы по внешним и Z-параметрам на указанной частоте.
        /// </summary>
        /// <param name="extrinsicParameters">Внешние (паразитные) элементы схемы.</param>
        /// <param name="sParameters">S-параметры.</param>
        /// <param name="freq">Частота расчета.</param>
        /// <returns>Внутренние элементы схемы.</returns>
        public List<double> GetIntrinsicParameters(List<double> extrinsicParameters,
            ComplexMatrix2x2 sParameters, double freq)
        {
            var yParams = SubstractExtrinsicParameters(sParameters, extrinsicParameters, freq);
            return CalculateIntrinsicParameters(yParams, freq);
        }

        /// <summary>
        /// Рассчитать параметры рассеяния модели для заданной частоты.
        /// </summary>
        /// <param name="extrinsicParameters">Внешние параметры.</param>
        /// /// <param name="intrinsicParameters">Внутренние параметры.</param>
        /// <param name="freq">Частота</param>
        /// <returns>Матрица S-параметров.</returns>
        public abstract ComplexMatrix2x2 GetSParameters(List<double> extrinsicParameters, List<double> intrinsicParameters,
            double freq);

		#endregion
    }
}
