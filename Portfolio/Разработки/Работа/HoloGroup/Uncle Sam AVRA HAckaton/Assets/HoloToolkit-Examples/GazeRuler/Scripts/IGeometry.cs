﻿using UnityEngine;
using System.Collections;

/// <summary>
/// interface for geometry class
/// </summary>
public interface IGeometry
{
    void AddPoint(GameObject LinePrefab, GameObject pointPrefab, GameObject textPrefab);

    void Delete();

    void Clear();

    void Reset();
}