// Unlit shader. Simplest possible textured shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "Unlit/ColorFadeInTransparent" {
Properties {
	_Color ("Color (RGBA)", Color) = (1,1,1,1)
	_FadeValue("Fade (0 - 1)", Range(0,1)) = 1
}

SubShader {
	Tags { "RenderType"="Transparent" "Queue" = "Transparent" }
	LOD 100
	
	Pass {
		ZWrite On
		ColorMask 0
	}
	
	Pass {
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata_t {
				float4 vertex : POSITION;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				UNITY_FOG_COORDS(1)
			};
			
			fixed4 _Color;
			half _FadeValue;
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = _Color;
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col * _FadeValue;
			}
		ENDCG
	}
}

}
