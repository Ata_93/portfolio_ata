﻿using System;
using System.Collections;
using System.Collections.Generic;
using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameFieldPlaneCreator : Singleton<GameFieldPlaneCreator>
    {
        private List<GameObject> _generatedPlanes;

        private GameObject _savedPlanesParent;

        public GameObject SelectedPlane { get; private set; }
        public event EventHandler OnLoaded;

        public void StartLoading()
        {
            SurfaceMeshesToPlanes.Instance.MakePlanesComplete += InstanceOnMakePlanesComplete;
            _savedPlanesParent = GameObject.Find("SavedPlanes");
            DontDestroyOnLoad(_savedPlanesParent);
            VoiceTipsManager.Instance.PlayTip(VoiceTips.ScanTheRoomTip);
            StartCoroutine(StartObserver());
        }

        private void InstanceOnMakePlanesComplete(object source, EventArgs args)
        {
            if (SurfaceMeshesToPlanes.Instance.GetActivePlanes(PlaneTypes.Floor | PlaneTypes.Table).Count < 1)
            {
                //SpatialMappingManager.Instance.St
                foreach (var instanceActivePlane in SurfaceMeshesToPlanes.Instance.ActivePlanes)
                    DestroyImmediate(instanceActivePlane);
                VoiceTipsManager.Instance.PlayTip(VoiceTips.RescanTheRoom);
                StartCoroutine(StartObserver());
            }
            else
            {
                Debug.Log("Enough");
                SurfaceMeshesToPlanes.Instance.MakePlanesComplete -= InstanceOnMakePlanesComplete;
                StopCoroutine(StartObserver());
                //SpatialMappingManager.Instance.StopObserver();
                
                _generatedPlanes = new List<GameObject>();
                foreach (var plane in SurfaceMeshesToPlanes.Instance.GetActivePlanes(PlaneTypes.Floor | PlaneTypes.Table))
                {
                    var surfacePlane = Instantiate(plane);
                    surfacePlane.transform.parent = _savedPlanesParent.transform;
                    _generatedPlanes.Add(surfacePlane);
                }

                foreach (var mesh in SpatialMappingManager.Instance.GetMeshFilters())
                {
                    DestroyImmediate(mesh.gameObject);
                }

                foreach (var instanceActivePlane in SurfaceMeshesToPlanes.Instance.ActivePlanes)
                {
                    DestroyImmediate(instanceActivePlane);
                }
                
                VoiceTipsManager.Instance.PlayTip(VoiceTips.ChoosePlane);
                SelectPlane();
            }
        }

        private IEnumerator StartObserver()
        {
            Debug.Log("Observer started");
            SpatialMappingManager.Instance.DrawVisualMeshes = true;
            if(!SpatialMappingManager.Instance.IsObserverRunning())
                SpatialMappingManager.Instance.StartObserver();
            yield return new WaitForSeconds(2f);
            SpatialMappingManager.Instance.StopObserver();
            SurfaceMeshesToPlanes.Instance.MakePlanes();
        }

        private void SelectPlane()
        {
            foreach (var instanceActivePlane in _generatedPlanes)
                instanceActivePlane.tag = "BattleField";
            TapSelectManager.Instance.SingleTap += InstanceOnOnTap;
        }

        private void InstanceOnOnTap(object sender, EventArgs eventArgs)
        {
            if (GazeController.Instance.FocusedObject.tag != "BattleField") return;
            SelectedPlane = GazeController.Instance.FocusedObject;
            TapSelectManager.Instance.SingleTap -= InstanceOnOnTap;
            foreach (var instanceActivePlane in _generatedPlanes)
                instanceActivePlane.SetActive(false);

            if (OnLoaded != null)
                OnLoaded.Invoke(this, EventArgs.Empty);
        }
    }
}