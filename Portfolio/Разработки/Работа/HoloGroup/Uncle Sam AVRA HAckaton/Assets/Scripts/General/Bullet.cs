﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Bullet : MonoBehaviour
    {
        public float Damage;

        void OnCollisionEnter()
        {
            Debug.Log("Collision!");
        }

        void OnDestroy()
        {
            Debug.Log("Destroyed");
        }
    }
}