﻿namespace Assets.Scripts
{
    public static class VoiceTips
    {
        public const string NoSpawnHere = "No spawn here.";

        public const string IntroTip = "Welcome, general!";

        public const string ScanTheRoomTip = "Look around to scan the room and find the spot for your army.";

        public const string RescanTheRoom = "Not enough planes for battlefield. Scan again.";

        public const string ChoosePlane = "Now, select the place ";

        public const string BattlefieldTip = "Here is battlefield. On the opposite side you can see the enemy team." +
                                             "To complete the level, you must eliminate all enemy soldiers. Look at any point of" +
                                             "the battlefield, and say Tank or Soldier to spawn Tank or Soldier. Say Are you ready to inspire your soldiers. " +
                                             "Say move to move forward. Say Attack to attack." +
                                             "Remember, you have the limit on spawn objects.";

        public const string Win = "You win, general!";
    }
}