﻿using Assets.Scripts.Units;
using UnityEngine;

namespace Assets.Scripts.Interfaces
{
    public class SpawnCommand : CommandAbstract
    {
        public string Name;

        public Vector3 LookPoint;

        public override void Execute()
        {
            
            //if()
            //if (t.GetComponent<UnitAbstract>() != null)
            {
                var t = Object.Instantiate(Resources.Load(Name)) as GameObject;
                //t.GetComponent<Rigidbody>().
                
                t.transform.position = new Vector3(LookPoint.x, ArmyObject.transform.position.y,
                    LookPoint.z);
                Debug.Log(t.transform.position);
                t.transform.parent = ArmyObject.transform;
                t.transform.localScale = Name == "Tank" ? new Vector3(10, 10, 10) : 
                    new Vector3(0.3f, 0.3f, 0.3f);
                ArmyObject.Army.Add(t.GetComponent<UnitAbstract>());
                
                var enemy = GameObject.Find("EnemyArmy");
                t.transform.LookAt(enemy.transform);
            }
        }
    }
}