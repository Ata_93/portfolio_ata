﻿using UnityEngine;

namespace Assets.Scripts.Interfaces
{
    public class GreetingsCommand : CommandAbstract
    {
        public override void Execute()
        {
            if (ArmyObject.Army.Count != 0)
            {
                ArmyObject.GetComponent<AudioSource>().Play();
                Debug.Log(ArmyObject.GetComponent<AudioSource>().isPlaying);
            }
            else Debug.Log("Not played!");
        }
    }
}