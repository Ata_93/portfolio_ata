﻿using System.Linq;
using Assets.Scripts.Units.Armies;

namespace Assets.Scripts.Interfaces
{
    public class MoveCommand : CommandAbstract
    {
        public override void Execute()
        {
            ArmyObject.Army.Where(t => t.IsSelected).ToList().ForEach(a => a.Move());
            
        }
    }
}