﻿namespace Assets.Scripts.Interfaces
{
    public interface IReceiver
    {
        void ReceiveAndExecute(CommandAbstract command);
    }
}