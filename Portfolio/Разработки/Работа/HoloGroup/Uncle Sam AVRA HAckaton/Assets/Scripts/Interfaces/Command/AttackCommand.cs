﻿using Assets.Scripts.Units.Armies;

namespace Assets.Scripts.Interfaces
{
    public class AttackCommand : CommandAbstract
    {
        public override void Execute()
        {
            ArmyObject.Army.ForEach(t => t.Attack());
        }
    }
}