﻿namespace Assets.Scripts.Interfaces
{
    public interface ISelectable
    {
        void Select();

        void Deselect();
    }
}