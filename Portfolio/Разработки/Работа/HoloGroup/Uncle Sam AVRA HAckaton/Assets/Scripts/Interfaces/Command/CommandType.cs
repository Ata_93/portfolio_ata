﻿namespace Assets.Scripts.Interfaces
{
    public enum CommandType
    {
        Move,
        Spawn,
        Attack,
        Hail
    }
}