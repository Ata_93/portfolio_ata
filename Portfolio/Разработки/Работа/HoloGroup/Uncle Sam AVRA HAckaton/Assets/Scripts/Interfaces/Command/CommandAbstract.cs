﻿using Assets.Scripts.Units.Armies;
using UnityEngine;

namespace Assets.Scripts.Interfaces
{
    public abstract class CommandAbstract
    {
        public abstract void Execute();

        public UserArmy ArmyObject;

        public CommandType Type;
    }
}