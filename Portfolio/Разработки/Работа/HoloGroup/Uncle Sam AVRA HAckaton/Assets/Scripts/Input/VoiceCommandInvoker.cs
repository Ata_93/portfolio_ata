﻿using System;
using Assets.Scripts;
using Assets.Scripts.Interfaces;
using HoloToolkit.Unity;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class VoiceCommandInvoker : Singleton<VoiceCommandInvoker>
{
    private KeywordRecognizer _recognizer;

    private IReceiver _currentReceiver;

    public void Initialize(IReceiver level)
    {
        _currentReceiver = level;
        _recognizer = new KeywordRecognizer(new string[] { "Tank", "Soldier", "Are you ready", "Move", "Attack" });
        _recognizer.OnPhraseRecognized += RecognizerOnPhraseRecognized;
        _recognizer.Start();
    }

    private void RecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        Debug.Log(args.text);
        switch (args.text)
        {
            case "Tank":
            {
                    if (GazeController.Instance.FocusedObject == null)
                        return;
                    if (!GazeController.Instance.FocusedObject.CompareTag("BattleField"))
                    {
                        VoiceTipsManager.Instance.PlayTip(VoiceTips.NoSpawnHere);
                        return;
                    }

                    _currentReceiver.ReceiveAndExecute(new SpawnCommand() { Type = CommandType.Spawn, Name = "Tank", LookPoint = GazeController.Instance.HitPoint });
            }
                break;
            case "Soldier":
            {
                    if (GazeController.Instance.FocusedObject == null)
                        return;
                    if (!GazeController.Instance.FocusedObject.CompareTag("BattleField"))
                    {
                        VoiceTipsManager.Instance.PlayTip(VoiceTips.NoSpawnHere);
                        return;
                    }
                    _currentReceiver.ReceiveAndExecute(new SpawnCommand() { Type = CommandType.Spawn, Name = "Soldier", LookPoint = GazeController.Instance.HitPoint });
                }
               
                break;
            case "Are you ready":
                _currentReceiver.ReceiveAndExecute(new GreetingsCommand() { Type = CommandType.Hail });
                break;
            case "Move":
                _currentReceiver.ReceiveAndExecute(new MoveCommand() { Type = CommandType.Move });
                break;
            case "Attack":
                _currentReceiver.ReceiveAndExecute(new AttackCommand() { Type = CommandType.Attack });
                break;
        }
    }

    void Update()
    {
        if (_currentReceiver == null) return;
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (GazeController.Instance.FocusedObject == null)
                return;
            if (!GazeController.Instance.FocusedObject.CompareTag("BattleField"))
            {
                VoiceTipsManager.Instance.PlayTip(VoiceTips.NoSpawnHere);
                return;
            }

            _currentReceiver.ReceiveAndExecute(new SpawnCommand() { Type = CommandType.Spawn, Name = "Tank", LookPoint = GazeController.Instance.HitPoint });
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (GazeController.Instance.FocusedObject == null)
                return;
            if (!GazeController.Instance.FocusedObject.CompareTag("BattleField"))
            {
                VoiceTipsManager.Instance.PlayTip(VoiceTips.NoSpawnHere);
                return;
            }
            _currentReceiver.ReceiveAndExecute(new SpawnCommand() { Type = CommandType.Spawn, Name = "Soldier", LookPoint = GazeController.Instance.HitPoint });
        }
        if (Input.GetKeyDown(KeyCode.C))
            _currentReceiver.ReceiveAndExecute(new GreetingsCommand() { Type = CommandType.Hail });
        if (Input.GetKeyDown(KeyCode.V))
            _currentReceiver.ReceiveAndExecute(new MoveCommand() { Type = CommandType.Move });
        if (Input.GetKeyDown(KeyCode.B))
            _currentReceiver.ReceiveAndExecute(new AttackCommand() { Type = CommandType.Attack });
    
    }

    void OnDestroy()
    {
        if (_recognizer != null)
        {
            _recognizer.OnPhraseRecognized -= RecognizerOnPhraseRecognized;
            _recognizer.Stop();
            _recognizer.Dispose();
        }
    }
}