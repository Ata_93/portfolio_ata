﻿using Assets.Scripts.Interfaces;
using HoloToolkit.Unity;
using UnityEngine;

namespace Assets.Scripts
{
    public class GazeController : Singleton<GazeController>
    {
        [SerializeField] private LayerMask _mask;

        private MeshRenderer _meshRenderer;

        private static RaycastHit _hitInfo;

        public GameObject FocusedObject { get; private set; }

        public Vector3 HitPoint
        {
            get { return _hitInfo.point; }
        }

        void Start()
        {
            _meshRenderer = GetComponent<MeshRenderer>();
        }

        void LateUpdate()
        {
            var gazeOrigin = Camera.main.transform.position;
            var gazeDirection = Camera.main.transform.forward;
            if (Physics.Raycast(gazeOrigin, gazeDirection, out _hitInfo, Mathf.Infinity, _mask))
            {
                _meshRenderer.enabled = true;

                transform.position = _hitInfo.point + _hitInfo.normal * 0.0001f;

                transform.rotation = Quaternion.FromToRotation(Vector3.back, _hitInfo.normal);
                if (FocusedObject != null)
                {
                    var deselect = FocusedObject.GetComponent<ISelectable>();
                    if (deselect != null)
                        deselect.Deselect();
                }
                
                FocusedObject = _hitInfo.collider.gameObject;
                var selectable = FocusedObject.GetComponent<ISelectable>();
                if (selectable != null)
                    selectable.Select();
            }
            else
            {
                if (FocusedObject == null) return;
                var selectable = FocusedObject.GetComponent<ISelectable>();
                if (selectable != null)
                    selectable.Deselect();
                FocusedObject = null;
                _meshRenderer.enabled = false;
                
            }
        }

    }
}