﻿using System;
using HoloToolkit.Unity;
using UnityEngine;
using UnityEngine.VR.WSA.Input;

namespace Assets.Scripts
{
    public class TapSelectManager : Singleton<TapSelectManager>
    {
        public event EventHandler SingleTap;

        private GestureRecognizer _recognizer;

        void Start()
        {
            _recognizer = new GestureRecognizer();
            _recognizer.SetRecognizableGestures(GestureSettings.Tap);
            _recognizer.TappedEvent += RecognizerOnTappedEvent;
            _recognizer.StartCapturingGestures();
        }

        private void RecognizerOnTappedEvent(InteractionSourceKind source, int tapCount, Ray headRay)
        {
            if(SingleTap != null)
                SingleTap.Invoke(this, EventArgs.Empty);
        }

        void Update()
        {
            if(Input.GetKeyDown(KeyCode.F))
            {
                if(SingleTap != null)
                    SingleTap.Invoke(this, EventArgs.Empty);
            }
        }
    }
}