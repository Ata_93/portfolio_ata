﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Interfaces;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Units
{
    public class UnitAbstract : MonoBehaviour, IUnit, ISelectable
    {

        [SerializeField] protected Material SelectMaterial;
        [SerializeField] protected Material DeselectMaterial;
        [SerializeField] protected Material EnemyMaterial;
        [SerializeField] protected GameObject Selection;

        protected float AttackRange;

        protected bool IsAttacking;

        private List<GameObject> _enemies;



        public bool IsSelected { get; protected set; }

        public int Rank { get; protected set; }

        public float Health { get; protected set; }
        public float MinDamage { get; protected set; }
        public float MaxDamage { get; protected set; }

        public bool IsEnemy { get; set; }

        public bool IsDead { get; protected set; }

        private void Die()
        {
            //DestroyImmediate(gameObject);

            GetComponentsInChildren<Renderer>().ToList().ForEach(t => t.enabled = false);
        }

        public virtual void Attack()
        {
            Debug.Log("Attack");
            if (IsAttacking)
                return;
            _enemies = GameObject.FindGameObjectsWithTag("EnemyUnit").ToList();
            var attackingUnit = _enemies[Random.Range(0, _enemies.Count)].GetComponent<UnitAbstract>();
            IsAttacking = true;
            StartCoroutine(AttackRoutine(attackingUnit));
        }

        protected IEnumerator AttackRoutine(UnitAbstract unit)
        {
            while (!unit.IsDead)
            {
                Debug.Log("Started to Unit = " + unit.name);
                var bullet = Instantiate(Resources.Load("Bullet")) as GameObject;
                if(bullet == null) yield break;
                var bulComp = bullet.GetComponent<Bullet>();
                bulComp.Damage = Random.Range(MinDamage, MaxDamage);
                bullet.transform.position = transform.position;
                bullet.transform.DOMove(unit.transform.position, 0.7f).OnComplete((() =>
                {
                    unit.GetDamage(bulComp.Damage);
                    Debug.Log("To target = " + unit.name);
                    bullet.SetActive(false);
                }));
                yield return new WaitForSeconds(0.9f);
                
            }
            if(_enemies.Where(t => !t.GetComponent<UnitAbstract>().IsDead).ToList().Count != 0)
                SwitchTarget();
            
        }

        protected void SwitchTarget()
        {
            try
            {
                var x = _enemies[Random.Range(0, _enemies.Count - 1)].GetComponent<UnitAbstract>();
                while(x.IsDead)
                    x = _enemies[Random.Range(0, _enemies.Count - 1)].GetComponent<UnitAbstract>();
                StartCoroutine(AttackRoutine(x));
            }
            catch (Exception)
            {
                Debug.Log("Exception!");
            }
        }

        public virtual void Move()
        {
            Debug.Log("Move");
            if (IsEnemy)
                return;
            var enemy = GameObject.Find("EnemyArmy");
            var reqPosition = Vector3.Distance(transform.position, enemy.transform.position);
            if (reqPosition <= 0.09)
                return;
            var t = transform.DOMove(transform.position + transform.forward*(reqPosition/10),0.5f).OnComplete((() =>
            {
                var direction = enemy.transform.position - transform.position;
                direction.y = 0;
                var targetRotation = Quaternion.LookRotation(direction);
                transform.rotation = targetRotation;
            }));
            
            t.Play();
        }

        public virtual void GetDamage(float damage)
        {
            Health -= damage;
            if (Health <= 0)
            {
                IsDead = true;
                Die();
            }
        }

        public void Select()
        {
            IsSelected = true;
            Selection.GetComponent<Renderer>().material = SelectMaterial;
        }

        public void Deselect()
        {
            IsSelected = false;
            Selection.GetComponent<Renderer>().material = IsEnemy ? EnemyMaterial : DeselectMaterial;
        }

        protected void OnDestroy()
        {
            StopAllCoroutines();
        }
    }
}