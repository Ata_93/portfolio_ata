﻿using UnityEngine;

namespace Assets.Scripts.Units
{
    public class Tank : UnitAbstract
    {
        void Awake()
        {
            Rank = 5;
            Health = 200;
            MinDamage = 4;
            MaxDamage = 8;
        }
    }
}