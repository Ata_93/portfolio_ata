﻿using UnityEngine;

namespace Assets.Scripts.Units
{
    public class Soldier : UnitAbstract
    {
        void Awake()
        {
            Rank = 1;
            Health = 105;
            MinDamage = 1;
            MaxDamage = 3;
        }
    }
}