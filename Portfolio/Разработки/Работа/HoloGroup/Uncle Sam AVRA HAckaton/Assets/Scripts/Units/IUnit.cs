﻿using JetBrains.Annotations;

namespace Assets.Scripts.Units
{
    public interface IUnit
    {
        int Rank { get; }

        float Health { get; }

        bool IsEnemy { get; set; }

        void Attack();

        void Move();

        void GetDamage(float damage);

    }
}