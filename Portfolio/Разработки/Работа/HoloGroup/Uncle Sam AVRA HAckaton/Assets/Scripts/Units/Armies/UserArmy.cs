﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Units.Armies
{
    public class UserArmy : MonoBehaviour
    {
        public List<UnitAbstract> Army { get; set; }

        void Awake()
        {
            Army = new List<UnitAbstract>();
        }

        public int CurrentRank
        {
            get { return Army.Select(t => t.Rank).Sum(); }
        }

       
        
    }
}