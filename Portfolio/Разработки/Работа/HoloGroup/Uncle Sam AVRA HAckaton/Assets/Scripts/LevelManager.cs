﻿using System;
using Assets.Scripts.Interfaces;
using HoloToolkit.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class LevelManager : Singleton<LevelManager>
    {
        private LevelAbstract _currentLevel;

        private Vector3 _boundSize;
        private MeshFilter _meshFilter;
        private float _minimumScale = 0.3f;
        private  Vector3 _boundsSize;

        [SerializeField] private Canvas Victory;

        public void LoadLevel(string levelName)
        {
            SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
            SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);
        }
        
        private void SceneManagerOnSceneLoaded(Scene arg0, LoadSceneMode loadSceneMode)
        {
            _currentLevel = GameObject.Find("LevelContent").GetComponent<LevelAbstract>();
            GameFieldPlaneCreator.Instance.SelectedPlane.SetActive(true);
            SetLevelToPlane();
            InitializeLevel();
        }

        private void SetLevelToPlane()
        {
            _currentLevel.gameObject.transform.position = GameFieldPlaneCreator.Instance.SelectedPlane.transform.position;
            var sizeX = GameFieldPlaneCreator.Instance.SelectedPlane.GetComponent<Renderer>().bounds.size.x * _currentLevel.transform.localScale.x 
                / _currentLevel.GetComponentInChildren<Renderer>().bounds.size.x;
            var sizeZ = GameFieldPlaneCreator.Instance.SelectedPlane.GetComponent<Renderer>().bounds.size.z * _currentLevel.transform.localScale.z
                / _currentLevel.GetComponentInChildren<Renderer>().bounds.size.z;
            _currentLevel.transform.localScale = new Vector3(sizeX, Math.Min(sizeX,sizeZ), sizeZ);
            //CoreServicesManager.Instance.SelectedPlane.SetActive(false);
        }

        private void InitializeLevel()
        {
            _currentLevel.Initialize();
            VoiceCommandInvoker.Instance.Initialize(_currentLevel.gameObject.GetComponent<IReceiver>());
            _currentLevel.OnLoaded += CurrentLevelOnEndGame;
        }

        private void CurrentLevelOnEndGame(object sender, EventArgs eventArgs)
        {
            //DestroyImmediate(_currentLevel);
            _currentLevel.gameObject.SetActive(false);
            Victory.GetComponent<CanvasGroup>().alpha = 1;
            GameFieldPlaneCreator.Instance.SelectedPlane.SetActive(false);
            VoiceTipsManager.Instance.PlayTip(VoiceTips.Win);
        }
    }
}