﻿using HoloToolkit.Unity;
using UnityEngine;

namespace Assets.Scripts
{
    public class VoiceTipsManager : Singleton<VoiceTipsManager>
    {
        [SerializeField] private TextToSpeechManager _tipsManager;

        public void PlayTip(string tip)
        {
            _tipsManager.SpeakText(tip);
        }

        public void StopTip()
        {
            _tipsManager.StopSpeaking();
        }
    }
}