﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Units;
using Assets.Scripts.Units.Armies;
using UnityEngine;

public abstract class LevelAbstract : MonoBehaviour, IReceiver
{
    protected List<IUnit> EnemyArmy;

    protected int MaxRank;

    protected List<CommandAbstract> Commands;

    public abstract void Initialize();
    public abstract event EventHandler OnLoaded;

    public virtual void ReceiveAndExecute(CommandAbstract command)
    {
        
    }
}