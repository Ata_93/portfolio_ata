﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Units;
using Assets.Scripts.Units.Armies;
using UnityEngine;

namespace Assets.Scripts.LevelManagers
{
    public class DemoLevel : LevelAbstract
    {
        [SerializeField] private GameObject _enemyArmy;

        [SerializeField]
        private UserArmy _army;

        private List<UnitAbstract> _enemyUnits;

        private const float ScaleFactorTank = 10f;

        private const float ScaleFactorSoldier = 0.3f;

        public override event EventHandler OnLoaded;
        

        public override void Initialize()
        {

            MaxRank = 15;
            Commands = new List<CommandAbstract>();
            _enemyUnits = _enemyArmy.GetComponentsInChildren<UnitAbstract>().ToList();
            _enemyUnits.ForEach(t =>
            {
                t.IsEnemy = true;
                t.gameObject.tag = "EnemyUnit";
                t.transform.localScale = t.name == "Tank" ?
                new Vector3(ScaleFactorTank, ScaleFactorTank, ScaleFactorTank):
                new Vector3(ScaleFactorSoldier,ScaleFactorSoldier,ScaleFactorSoldier);
            });
            TapSelectManager.Instance.SingleTap += InstanceOnSingleTap;
            Debug.Log("Level Init");
            VoiceTipsManager.Instance.PlayTip(VoiceTips.BattlefieldTip);
        }

        private void InstanceOnSingleTap(object sender, EventArgs eventArgs)
        {
            VoiceTipsManager.Instance.StopTip();
            TapSelectManager.Instance.SingleTap -= InstanceOnSingleTap;
        }

        public override void ReceiveAndExecute(CommandAbstract command)
        {
            if (command.Type == CommandType.Spawn)
                if (_army.CurrentRank >= MaxRank)
                    return;
            command.ArmyObject = _army;
            Commands.Add(command);
            command.Execute();
        }

        void Update()
        {
            if (_enemyUnits.Where(t => !t.IsDead).ToList().Count == 0)
            {
                if (OnLoaded != null)
                {
                    OnLoaded.Invoke(this, EventArgs.Empty);
                }
            }
              
                
        }
    }
}