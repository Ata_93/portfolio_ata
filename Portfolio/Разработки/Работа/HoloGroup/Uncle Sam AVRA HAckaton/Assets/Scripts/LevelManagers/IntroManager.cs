﻿using System.Collections;
using Assets.Scripts;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;


public class IntroManager : MonoBehaviour
{
    [SerializeField] private GameObject _logo;

    private void Awake()
    {
        SceneManager.LoadSceneAsync("CoreServices", LoadSceneMode.Additive);
    }

    private IEnumerator Start()
    {
        while (Camera.main == null)
            yield return null;
        _logo.transform.parent = Camera.main.transform;
        _logo.transform.localPosition = Vector3.zero;
        VoiceTipsManager.Instance.PlayTip(VoiceTips.IntroTip);
        yield return new WaitForSeconds(3.5f);
        DestroyImmediate(_logo);
        GameFieldPlaneCreator.Instance.OnLoaded += delegate
        {
            SceneManager.UnloadScene("Intro");
            LevelManager.Instance.LoadLevel("DemoLevel");
        };
        GameFieldPlaneCreator.Instance.StartLoading();
    }


    
    
}