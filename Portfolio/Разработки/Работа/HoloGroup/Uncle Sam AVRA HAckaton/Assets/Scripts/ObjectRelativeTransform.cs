﻿using UnityEngine;

namespace Assets.Scripts.General
{
    /// <summary>
    ///     Script similar to TagAlong, except it allows object to follow camera and keep limited position by Y.
    /// </summary>
    public class ObjectRelativeTransform : MonoBehaviour
    {
        [SerializeField] private float _direction;
        [SerializeField] private bool _isFolowingCamera;

        [Range(0, 1)] [SerializeField] private float _yRadius;

        void Update()
        {
            if (Camera.main == null)
                return;
            var directionToTarget = Camera.main.transform.position - gameObject.transform.position;

            if (directionToTarget.sqrMagnitude < 0.001f)
                return;

            directionToTarget.y = 0.0f;
            gameObject.transform.rotation = Camera.main.transform.rotation;
            gameObject.transform.rotation = Quaternion.LookRotation(-directionToTarget);
            if (_isFolowingCamera)
                transform.position = Camera.main.transform.position + Camera.main.transform.forward*_direction;
            Debug.Log(transform.position);
        }
    }
}