﻿using System;
using UnityEngine;
using UnityEditor;

public class Builder : MonoBehaviour {

    [MenuItem("Uncle Sam/Build")]
    public static void BuildStandalone()
    {
        string errorMessage = string.Empty;
        try
        {
            var path = "uncle_sam";
            var levels = new[] { "Assets/Scenes/Intro.unity", "Assets/Scenes/CoreServices.unity", "Assets/Scenes/DemoLevel.unity" };
            PlayerSettings.productName = "Uncle Sam";
            PlayerSettings.WSA.packageName = "Uncle Sam";
            PlayerSettings.WSA.tileShortName = "Uncle Sam";
            QualitySettings.SetQualityLevel(0);
            errorMessage = BuildPipeline.BuildPlayer(levels, path + "/Standalone", BuildTarget.WSAPlayer, BuildOptions.Development);
        }
        catch (Exception e)
        {
            Debug.LogError("Error Building");
            Debug.LogError(e.Message);
            Debug.LogError(errorMessage);
        }
    }
}
