﻿namespace Assets.Scripts.ScenesManagers
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class EntryPointManager : MonoBehaviour
    {
        [SerializeField] private string _nextScene = "Main";

        [SerializeField] private float _timeToLoad = 3;

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene(_nextScene, LoadSceneMode.Single);
        }
        
        
    }
}