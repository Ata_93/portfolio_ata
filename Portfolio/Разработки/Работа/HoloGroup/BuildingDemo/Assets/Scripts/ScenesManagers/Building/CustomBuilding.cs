﻿namespace Assets.Scripts.ScenesManagers.Building
{
    using System.Collections.Generic;
    using System.Linq;
    using General.Library;
    using HoloToolkit.Unity;
    using UnityEngine;

    public class CustomBuilding : MonoBehaviour
    {
        private Vector3 _positionBeforeInside;

        private float _scaleBeforeInside;

        private BoxCollider _collider;
        
        protected Vector3 DefaultPosition;

        protected Quaternion DefaultRotation;

        protected Vector3 DefaultScale;

        protected GameObject Floor;

        protected GameObject Roof;

        protected List<GameObject> Furniture;

        protected GameObject SpawnPoint;

        public bool HasRoof { get { return Roof != null; } }

        public bool HasFurniture { get { return Furniture != null; } }

        public bool HasFloor { get { return Floor != null; } }
        
        protected virtual void Awake()
        {
            Floor = GameObject.FindGameObjectWithTag("Floor");
            Roof = GameObject.FindGameObjectWithTag("Roof");
            Furniture = GameObject.FindGameObjectsWithTag("Furniture").ToList();
            SpawnPoint = GameObject.FindGameObjectWithTag("SpawnPoint");

            DefaultPosition = gameObject.transform.localPosition;
            DefaultRotation = gameObject.transform.localRotation;
            DefaultScale = gameObject.transform.localScale;

            var bounds = gameObject.GetBounds();
            _collider = gameObject.AddComponent<BoxCollider>();
            _collider.size = bounds.size;
            _collider.center = bounds.center;

            Floor.SetActive(true);
            Roof.SetActive(true);
            Furniture.ForEach(g => g.SetActive(false));
        }

        public virtual void Outside()
        {
            gameObject.transform.position = _positionBeforeInside;
            gameObject.transform.localScale = new Vector3(_scaleBeforeInside,_scaleBeforeInside, _scaleBeforeInside);
            _collider.enabled = true;
        }

        public void Inside()
        {
            var floorReal = SurfaceMeshesToPlanes.Instance.FloorYPosition;
            Debug.Log("Floor position = " + floorReal);
            var modelFloorHitPoint = SpawnPoint.transform.position;

            RaycastHit modelFloorHitInfo;
            if (Physics.Raycast(Camera.main.transform.position, 
                Camera.main.transform.forward, out modelFloorHitInfo, 12f, LayerMask.GetMask("Floor")))
            {
                modelFloorHitPoint = modelFloorHitInfo.point;
            }

            _positionBeforeInside = transform.position;
            _scaleBeforeInside = transform.localScale.x;

            var v1 = modelFloorHitPoint - transform.position;
            var v1Scaled = v1 * (1f / _scaleBeforeInside);

            var targetPosition = Camera.main.transform.position - v1Scaled + 
                (new Vector3(Camera.main.transform.position.x, floorReal, Camera.main.transform.position.z) -
                Camera.main.transform.position);

            transform.localScale = new Vector3(1f, 1f, 1f);
            transform.position = targetPosition;
            _collider.enabled = false;
        }

        public void ChangeRoofVisibility(bool activeSelf)
        {
            Roof.SetActive(activeSelf);
            if (activeSelf)
                ChangeFurnitureVisibility(false);
        }

        public void ChangeFurnitureVisibility(bool isActive)
        {
            Furniture.ForEach(g => g.SetActive(isActive));
        }

        public void SetDefault()
        {
            gameObject.transform.localPosition = DefaultPosition;
            gameObject.transform.localRotation = DefaultRotation;
            gameObject.transform.localScale = DefaultScale;
        }
        
    }
}