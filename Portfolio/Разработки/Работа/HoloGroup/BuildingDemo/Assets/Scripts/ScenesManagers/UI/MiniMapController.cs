﻿namespace Assets.Scripts.ScenesManagers.UI
{
    using UnityEngine;

    public class MiniMapController : MonoBehaviour
    {
        [SerializeField] private GameObject _userMark;

        public void Show()
        {
            gameObject.SetActive(true);
            enabled = true;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            enabled = false;
        }

        private void Update()
        {
            _userMark.transform.position = Camera.main.transform.position;
            _userMark.transform.rotation = Camera.main.transform.rotation;
        }
    }
}