﻿namespace Assets.Scripts.ScenesManagers
{
    using System.Collections;
    using Assets.Scripts.General;
    using Assets.Scripts.General.Library;
    using ScenesManagers.Building;
    using Assets.Scripts.ScenesManagers.UI;
    using HoloToolkit.Unity;
    using UnityEngine;

    public class MainSceneManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject _currentBuildingModel;

        [SerializeField]
        private MiniMapController _minimap;

        private CustomBuilding _building;

        private ObjectPlacer _placementPrefab;

        private bool _isInPlacementMode;
        
        private bool _isInside;

        private bool _isRoofShown;

        private bool _isFurnitureShown;
        
        private void Awake()
        {
            _currentBuildingModel.SetActive(false);
        }

        private IEnumerator Start()
        {
            SpatialMappingManager.Instance.StartObserver();
            HintAndError.ShowHint("Scan the room", 5);
            yield return new WaitForSeconds(10.0f);
            SpatialMappingManager.Instance.StopObserver();

            SurfaceMeshesToPlanes.Instance.MakePlanesComplete += delegate
            {
                SpatialMappingManager.Instance.drawVisualMeshes = false;
                StartPlacingObject();
            };
            SurfaceMeshesToPlanes.Instance.MakePlanes();
            RemoveSurfaceVertices.Instance.RemoveSurfaceVerticesWithinBounds(SurfaceMeshesToPlanes.Instance.ActivePlanes);
        }

        private void StartPlacingObject()
        {
            if (_placementPrefab == null)
                CreatePlacementPrefab();
            _placementPrefab.IsPlacing = true;
            _isInPlacementMode = true;
            HintAndError.ShowHint("Place object on horizontal plate");
        }

        private void CreatePlacementPrefab()
        {
            var objectBounds = _currentBuildingModel.GetBounds().size ;
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.GetComponent<Renderer>().material = Resources.Load("SimpleMaterial") as Material;
            cube.transform.position = Camera.main.transform.TransformPoint(0, 0, 5);
            cube.transform.rotation = Quaternion.identity;;
            var cubeBounds = cube.GetBounds();
            cube.transform.localScale = new Vector3(objectBounds.x / cubeBounds.size.x,
                objectBounds.y / cubeBounds.size.y, objectBounds.z / cubeBounds.size.z);
            _placementPrefab = cube.AddComponent<ObjectPlacer>();
        }

        public void StopPlacingObject()
        {
            _isInPlacementMode = false;
            Debug.Log("1");
            if (_placementPrefab != null)
                _placementPrefab.IsPlacing = false;
            if (_building == null)
            {
                _currentBuildingModel.SetActive(true);
                _currentBuildingModel.transform.position = _placementPrefab.transform.position;
                _currentBuildingModel.transform.rotation = _placementPrefab.transform.rotation;
                _placementPrefab.gameObject.SetActive(false);
                _currentBuildingModel.AddComponent<Transformer>();
                _building = _currentBuildingModel.AddComponent<CustomBuilding>();
            }
        }

        public void ChangeInsideState(bool flag)
        {
            if (_isInPlacementMode)
                return;
            if (flag == _isInside)
            {
                HintAndError.ShowError(!flag ? "Already outside" : "Already inside");
                return;
            }
            if(!flag)
                _building.Outside();
            else 
                _building.Inside();
            _isInside = flag;
            if(_isInside)
                _minimap.Show();
            else
                _minimap.Hide();
        }

        public void ChangeRoofVisibility(bool flag)
        {
            if (_isInPlacementMode)
                return;
            if (!_building.HasRoof)
            {
                HintAndError.ShowError("No roof object found");
                return;
            }
            if (_isRoofShown == flag)
            {
                var shown = _isRoofShown ? "Showed" : "Hidden";
                HintAndError.ShowError("Roof is already " + shown);
                return;
            }
            _isRoofShown = flag;
            if (_isRoofShown)
                _building.ChangeFurnitureVisibility(false);
            _building.ChangeRoofVisibility(flag);
        }

        public void ChangeFurnitureVisibility(bool flag)
        {
            if (_isInPlacementMode)
                return;
            if (!_building.HasFurniture)
            {
                HintAndError.ShowError("Building has no furniture!");
                return;
            }

            if (_isRoofShown)
            {
                HintAndError.ShowError("Furniture is only available when roof is hidden");
                return;
            }
            if (_isFurnitureShown == flag)
            {
                var shown = _isFurnitureShown ? "Showed" : "Hidden";
                HintAndError.ShowError("Furniture is already " + shown);
                return;
            }
            _isFurnitureShown = flag;
            _building.ChangeFurnitureVisibility(flag);
        }

        public void ReturnToDefaultState()
        {
            if (_isInPlacementMode)
                return;
            _building.SetDefault();
        }

    }
}