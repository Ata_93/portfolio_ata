﻿namespace Assets.Scripts.DebugInfo
{
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    ///     Simple Behaviour which calculates the frames per seconds and shows the FPS in a referenced Text control.
    /// </summary>
    public class FpsDisplay : MonoBehaviour
    {
        private float _deltaTime;

        private double accum; // FPS accumulated over the interval
        private int frames; // Frames drawn over the interval

        [Tooltip("Reference to Text UI control where the FPS should be displayed.")] public Text Text;

        private float timeleft; // Left time for current interval

        public float updateInterval = 3f;

        private void Start()
        {
            timeleft = updateInterval;
        }

        private void Update()
        {
#if !DEMO
            timeleft -= Time.deltaTime;
            accum += Time.timeScale/Time.deltaTime;
            ++frames;

            // Interval ended - update GUI text and start new interval
            if (timeleft <= 0.0)
            {
                // display two fractional digits (f2 format)
                var fps = (int) (accum/frames);
                Text.text = string.Format("FPS: {0}", fps);
                timeleft = updateInterval;
                accum = 0.0;
                frames = 0;
            }
#endif
        }
    }
}