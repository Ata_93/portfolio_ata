﻿namespace Assets.Scripts.DebugInfo
{
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// Log information on screen canvas
    /// </summary>
    public static class ScreenLog
    {
        public static void Log(string text)
        {
#if !DEMO
            var debugInfo = GameObject.FindGameObjectWithTag("Loger");
            if (debugInfo == null)
                return;
            var textControl = debugInfo.GetComponent<Text>();
            var textLines = textControl.text.Split('\n');
            var currentText = textControl.text;
            if (textLines.Length >= 10)
                textControl.text = currentText.Remove(0, textLines[0].Length + 1);
            textControl.text += text + "\n";
#endif
        }
    }
}