﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.General.Library;
using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// Класс, позволяющий "расчленить" объект на составные детали.
    /// Если составные части не обозначены пользователем, то деталями считаются все дочернии объекты в иерархии GameObject.
    /// </summary>
    [DisallowMultipleComponent]
    public class Dismemberer : MonoBehaviour
    {
        private struct TransformBeforeDecomposing
        {
            public readonly Vector3 Position;

            public readonly Quaternion Rotation;

            public readonly Vector3 Scale;

            public readonly Transform TransformParent;

            public TransformBeforeDecomposing(Vector3 position, Quaternion rotation, Vector3 scale, Transform transformParent)
            {
                Position = position;
                Rotation = rotation;
                Scale = scale;
                TransformParent = transformParent;
            }
        }
        
        private BoxCollider _collider;

        private short _id;
        
        private List<Vector3> _positionsNew;

        private List<TransformBeforeDecomposing> _transforms;

        private List<Bounds> _bounds = new List<Bounds>();

        [SerializeField]
        private List<GameObject> _parts;

        public bool IsDismembered { get; private set; }

        private void Awake()
        {   
            _transforms = new List<TransformBeforeDecomposing>();
            _id = 3001;
            Debug.Log("Dismember start");
            IsDismembered = false;
            if(_parts.Count == 0)
                _parts.AddRange(GetComponentsInChildren<Transform>().Select(t => t.gameObject).Where(s => s.gameObject != gameObject));
            foreach (var part in _parts)
            {
                var totalBounds = part.GetBounds();
                _bounds.Add(totalBounds);
                var bc = part.GetComponent<BoxCollider>();
                if (bc == null)
                    bc = part.AddComponent<BoxCollider>();
                bc.enabled = false;
                bc.isTrigger = true;
                bc.size = (1 / transform.localScale.x)*totalBounds.size;
                bc.center = Vector3.zero;
            }
        }

        private void CalculateObjectPositions()
        {
            _positionsNew = new List<Vector3>();
            var interval = 0.25f;
            var centerOfSequence = gameObject.transform.position;
            var hasElementInMiddle = _parts.Count % 2 != 0;
            int middleElementIndex = hasElementInMiddle ? _parts.Count / 2 : -1;

            if (hasElementInMiddle)
                _positionsNew.Add(new Vector3(centerOfSequence.x, Camera.main.transform.position.y, centerOfSequence.z));

            int lowArrayBorder = hasElementInMiddle ? middleElementIndex - 1 : Mathf.FloorToInt(_parts.Count / 2.0f);
            int highArrayBorder = hasElementInMiddle ? middleElementIndex + 1 : Mathf.CeilToInt(_parts.Count / 2.0f);

            var currentPosition = centerOfSequence;
            for (int i = lowArrayBorder; i >= 0; i--)
            {
                currentPosition = new Vector3(currentPosition.x - transform.localScale.x * _parts[i].GetComponent<BoxCollider>().size.x - interval,
                    Camera.main.transform.position.y, centerOfSequence.z);
                _positionsNew.Insert(0, currentPosition);

            }
            currentPosition = centerOfSequence;
            for (int j = highArrayBorder; j < _parts.Count; j++)
            {
                currentPosition = new Vector3(
                    currentPosition.x + transform.localScale.x * _parts[j].GetComponent<BoxCollider>().size.x + interval,
                    Camera.main.transform.position.y, centerOfSequence.z);
                _positionsNew.Insert(j, currentPosition);
            }
        }
        
        public void Dismember()
        {
            if (IsDismembered)
                return;

            if (_collider == null)
                _collider = GetComponent<BoxCollider>();

            if (_collider != null)
                _collider.enabled = false;
            CalculateObjectPositions();
            for (var index = 0; index < _parts.Count; index++)
            {
                var part = _parts[index];
                part.transform.rotation = Quaternion.identity;
                var transformBeforeSlicing = new TransformBeforeDecomposing(part.transform.position,
                    part.transform.rotation,
                    part.transform.localScale,
                    part.transform.parent);
                _transforms.Add(transformBeforeSlicing);
                part.GetComponent<BoxCollider>().enabled = true;
                part.transform.position = _positionsNew[index];
                _id++;
            }
            IsDismembered = true;
        }
        
        public void ReturnToFull()
        {
            if (!IsDismembered)
                return;
            for (var index = 0; index < _parts.Count; index++)
            {
                var part = _parts[index];
                var joints = part.GetComponents<FixedJoint>().ToList();
                if (joints != null)
                    joints.ForEach(t =>
                    {
                        t.connectedBody = null;
                        DestroyImmediate(t);
                    });
                var body = part.GetComponent<Rigidbody>();
                if (body != null)
                    DestroyImmediate(body);
                part.GetComponent<BoxCollider>().enabled = false;
                part.transform.parent = _transforms[index].TransformParent;
                part.transform.position = _transforms[index].Position;
                part.transform.rotation = _transforms[index].Rotation;
                part.transform.localScale = _transforms[index].Scale;
            }
            _transforms.Clear();
            _collider.enabled = true;
            IsDismembered = false;
            _id = 3001;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
                Dismember();
            if (Input.GetKeyDown(KeyCode.E))
                ReturnToFull();

        }
    }
}