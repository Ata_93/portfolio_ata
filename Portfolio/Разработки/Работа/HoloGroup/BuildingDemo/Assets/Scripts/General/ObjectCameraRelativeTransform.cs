﻿namespace Assets.Scripts.General
{
    using Assets.Scripts.General.Library;
    using UnityEngine;

    /// <summary>
    ///     Script similar to TagAlong, except it allows object to follow camera and keep limited position by Y.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class ObjectCameraRelativeTransform : MonoBehaviour
    {
        [Tooltip("Is folowing Camera")]
        [SerializeField]
        private bool _isFolowingCamera;

        [Tooltip("Position from camera")]
        [SerializeField]
        private Vector3 _position;

        [Tooltip("Y angle limit")]
        [Range(0, 60)]
        [SerializeField]
        private float _yRotationLimit;

        [SerializeField]
        private bool _isLokedInFOV;

        private float _maxFOVAngle;

        private Bounds _bounds;

        private void Awake()
        {
            _bounds = gameObject.GetBounds();
            _maxFOVAngle = Vector3.Angle(_bounds.center, _bounds.max);
        }

        private void Update()
        {
            if (Camera.main == null)
                return;
            var directionToTarget = Camera.main.transform.position - gameObject.transform.position;
            
            var angle = Camera.main.transform.rotation.eulerAngles.x;
            if (angle > _yRotationLimit && angle < 360 - _yRotationLimit)
                return;

            if (_isLokedInFOV)
            {
                if ((Quaternion.Angle(Quaternion.LookRotation(transform.position - Camera.main.transform.position), Camera.main.transform.rotation)
                    < _maxFOVAngle))
                    return;
            }
            
            gameObject.transform.rotation = Quaternion.LookRotation(-directionToTarget);
            if (_isFolowingCamera)
            {
                //transform.position = Camera.main.transform.position + Camera.main.transform.forward * _position.z;
                transform.position = Camera.main.transform.TransformPoint(_position);
                                     //Quaternion.Euler(0, Camera.main.transform.rotation.eulerAngles.y, 0) *
                                     //Vector3.Scale(Camera.main.transform.lossyScale, _position);

            }
        }


    }
}