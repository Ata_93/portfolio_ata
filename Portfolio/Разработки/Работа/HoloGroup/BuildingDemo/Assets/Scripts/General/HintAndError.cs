﻿namespace Assets.Scripts.General
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.UI;

    public static class HintAndError
    {
        private static GameObject _hintAndError;

        public static void ShowHint(string text, int sec = 2)
        {
            if(_hintAndError == null)
                _hintAndError = GameObject.Find("Hint");
            _hintAndError.GetComponent<Text>().color = Color.white;
            _hintAndError.GetComponent<MonoBehaviour>().StartCoroutine(StartShowingMessageCoroutine(text, sec));
        }

        public static void ShowError(string text, int sec = 3)
        {
            if (_hintAndError == null)
                _hintAndError = GameObject.Find("Hint");
            _hintAndError.GetComponent<Text>().color = Color.red;
            _hintAndError.GetComponent<MonoBehaviour>().StartCoroutine(StartShowingMessageCoroutine(text, sec));
        }

        private static IEnumerator StartShowingMessageCoroutine(string text, int time)
        {
            _hintAndError.GetComponent<Text>().text = text;
            yield return new WaitForSeconds(time);
            _hintAndError.GetComponent<Text>().text = "";
        }
    }
}