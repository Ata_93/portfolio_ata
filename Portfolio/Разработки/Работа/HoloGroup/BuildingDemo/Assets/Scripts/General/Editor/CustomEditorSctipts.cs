﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEditor;

public class CustomEditorSctipts : MonoBehaviour
{
    [MenuItem("CustomEditor/Set Icons")]
    public static void SetPlayerIcons()
    {
        string errorMessage = string.Empty;
        try
        {
            //71x71 logos
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/71x71.png", PlayerSettings.WSAImageType.UWPSquare71x71Logo, PlayerSettings.WSAImageScale._100);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/89x89.png", PlayerSettings.WSAImageType.UWPSquare71x71Logo, PlayerSettings.WSAImageScale._125);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/107x107.png", PlayerSettings.WSAImageType.UWPSquare71x71Logo, PlayerSettings.WSAImageScale._150);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/142x142.png", PlayerSettings.WSAImageType.UWPSquare71x71Logo, PlayerSettings.WSAImageScale._200);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/284x284.png", PlayerSettings.WSAImageType.UWPSquare71x71Logo, PlayerSettings.WSAImageScale._400);

            //150x150
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/150x150.png", PlayerSettings.WSAImageType.UWPSquare150x150Logo, PlayerSettings.WSAImageScale._100);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/300x300.png", PlayerSettings.WSAImageType.UWPSquare150x150Logo, PlayerSettings.WSAImageScale._200);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/600x600.png", PlayerSettings.WSAImageType.UWPSquare150x150Logo, PlayerSettings.WSAImageScale._400);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/188x188.png", PlayerSettings.WSAImageType.UWPSquare150x150Logo, PlayerSettings.WSAImageScale._125);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/225x225.png", PlayerSettings.WSAImageType.UWPSquare150x150Logo, PlayerSettings.WSAImageScale._150);

            //310x310
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/310x310.png", PlayerSettings.WSAImageType.UWPSquare310x310Logo, PlayerSettings.WSAImageScale._100);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/620x620.png", PlayerSettings.WSAImageType.UWPSquare310x310Logo, PlayerSettings.WSAImageScale._200);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/1240x1240.png", PlayerSettings.WSAImageType.UWPSquare310x310Logo, PlayerSettings.WSAImageScale._400);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/465x465.png", PlayerSettings.WSAImageType.UWPSquare310x310Logo, PlayerSettings.WSAImageScale._150);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/388x388.png", PlayerSettings.WSAImageType.UWPSquare310x310Logo, PlayerSettings.WSAImageScale._125);

            //310x150
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/310x150.png", PlayerSettings.WSAImageType.UWPWide310x150Logo, PlayerSettings.WSAImageScale._100);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/620x300.png", PlayerSettings.WSAImageType.UWPWide310x150Logo, PlayerSettings.WSAImageScale._200);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/1240x600.png", PlayerSettings.WSAImageType.UWPWide310x150Logo, PlayerSettings.WSAImageScale._400);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/388x188.png", PlayerSettings.WSAImageType.UWPWide310x150Logo, PlayerSettings.WSAImageScale._125);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/465x225.png", PlayerSettings.WSAImageType.UWPWide310x150Logo, PlayerSettings.WSAImageScale._150);

            //Store logo
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/75x75.png", PlayerSettings.WSAImageType.PackageLogo, PlayerSettings.WSAImageScale._150);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/100x100.png", PlayerSettings.WSAImageType.PackageLogo, PlayerSettings.WSAImageScale._200);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/50x50.png", PlayerSettings.WSAImageType.PackageLogo, PlayerSettings.WSAImageScale._100);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/63x63.png", PlayerSettings.WSAImageType.PackageLogo, PlayerSettings.WSAImageScale._125);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/200x200.png", PlayerSettings.WSAImageType.PackageLogo, PlayerSettings.WSAImageScale._400);

            //Square 44x44
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/44x44.png", PlayerSettings.WSAImageType.UWPSquare44x44Logo, PlayerSettings.WSAImageScale._100);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/88x88.png", PlayerSettings.WSAImageType.UWPSquare44x44Logo, PlayerSettings.WSAImageScale._200);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/176x176.png", PlayerSettings.WSAImageType.UWPSquare44x44Logo, PlayerSettings.WSAImageScale._400);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/55x55.png", PlayerSettings.WSAImageType.UWPSquare44x44Logo, PlayerSettings.WSAImageScale._125);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/66x66.png", PlayerSettings.WSAImageType.UWPSquare44x44Logo, PlayerSettings.WSAImageScale._150);

            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/16x16.png", PlayerSettings.WSAImageType.UWPSquare44x44Logo, PlayerSettings.WSAImageScale.Target16);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/24x24.png", PlayerSettings.WSAImageType.UWPSquare44x44Logo, PlayerSettings.WSAImageScale.Target24);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/48x48.png", PlayerSettings.WSAImageType.UWPSquare44x44Logo, PlayerSettings.WSAImageScale.Target48);
            PlayerSettings.WSA.SetVisualAssetsImage("Assets/Icons/256x256.png", PlayerSettings.WSAImageType.UWPSquare44x44Logo, PlayerSettings.WSAImageScale.Target256);

        }
        catch (Exception e)
        {
            Debug.LogError("Error");
            Debug.LogError("Specify Icons in Assets/Icons path");
            Debug.LogError(e.Message);
            Debug.LogError(errorMessage);
        }
    }

    [MenuItem("CustomEditor/Calculate Code Lines")]
    public static void CalculateCodeLineNumbers()
    {
        string directory = Application.dataPath; //Solution Directory.
        var directoryInfo = new DirectoryInfo(directory);
        int amount = directoryInfo.GetFiles("*", SearchOption.AllDirectories).
            Where(s => s.Extension == ".cs" && !s.DirectoryName.Contains("HoloToolkit")).
            ToList().Where(t => t != null).
            Select(s => File.ReadAllLines(s.DirectoryName + "//" + s.Name).Length).
            ToList().Sum();
        EditorUtility.DisplayDialog(PlayerSettings.productName, "Total Code Lines in project = " + amount, "OK");
    }


}
