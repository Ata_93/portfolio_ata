﻿namespace Assets.Scripts.General.Library
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    /// <summary>
    /// Extension Methods for GameObject.
    /// </summary>
    public static class GameObjectExtentionMethods
    {
        /// <summary>
        /// Calculate Object Bounds Size.
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns>Bounds</returns>
        public static Bounds GetBounds(this GameObject gameObject)
        {
            var totalBounds = new Bounds();

            var mrs = gameObject.GetComponentsInChildren<MeshRenderer>();

            if (mrs.Length > 0)
            {
                totalBounds = mrs[0].bounds;

                for (int i = 1; i < mrs.Length; i++)
                {
                    totalBounds.Encapsulate(mrs[i].bounds);
                }
            }
            else
            {
                totalBounds.center = gameObject.transform.position;
            }
            return totalBounds;
        }

        /// <summary>
        /// Get Next to element GameObject
        /// </summary>
        /// <param name="list"></param>
        /// <param name="element"></param>
        /// <returns></returns>
        public static GameObject Next(this List<GameObject> list, GameObject element)
        {
            element.SetActive(false);
            var next = list[list.IndexOf(element) == list.Count - 1 ? 0 : list.IndexOf(element) + 1];
            next.SetActive(true);
            return next;
        }

        /// <summary>
        /// Change alpha of all materials attached to gameobject
        /// </summary>
        /// <param name="gameObject"></param>
        /// <param name="value"></param>
        public static void ChangeMaterialsAlpha(this GameObject gameObject, float value)
        {
            var materials = gameObject.GetComponentsInChildren<Renderer>().SelectMany(t => t.materials).ToList();
            foreach (var material in materials)
            {
                if (material.HasProperty("_Color"))
                {
                    var c = material.color;
                    c.a = value;
                    material.color = c;
                }
                else
                {
                    var propertyName = material.HasProperty("_Alpha") ? "_Alpha" : "_T";
                    material.SetFloat(propertyName, value);
                }
            }
        }

        /// <summary>
        /// Change alpha of single material.
        /// </summary>
        /// <param name="gameObject"></param>
        /// <param name="value">New alpha value</param>
        public static void ChangeSingleMaterialAlpha(this GameObject gameObject, float value)
        {
            var material = gameObject.GetComponent<Renderer>().material;
            if (material.HasProperty("_Color"))
            {
                var c = material.color;
                c.a = value;
                material.color = c;
            }
            else
            {
                var propertyName = material.HasProperty("_Alpha") ? "_Alpha" : "_T";
                material.SetFloat(propertyName, value);
            }
        }

        /// <summary>
        /// Get material color.
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public static Color GetMaterialColor(this GameObject go)
        {
            return go.GetComponent<Renderer>().material.color;
        }
    }
}