﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.

using UnityEngine;

namespace Assets.Scripts.Custom
{
    /// <summary>
    /// Custom singleton implementation.
    /// HoloToolkit singleton lacks flexibility,
    /// So it's recommended to use this class.
    /// <strong>WARNING!</strong> If you attempt to call singleton in OnDestroy methods.
    /// Check IsAlive flag first!
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        private static T _instance;

        /// <summary>
        /// Empty object for implementing thread-safe singleton;
        /// </summary>
        private static readonly object _lock = new object();

        /// <summary>
        /// Check if instance still exists.
        /// </summary>
        public static bool IsAlive { get { return _instance == null; } }

        /// <summary>
        /// Objects instance.
        /// </summary>
        public static T Instance
        {
            get
            {
                if (_isAlive)
                {
                    Debug.LogWarning("Singleton was already destroyed");
                    return null;
                }

                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = FindObjectOfType<T>();

                        if (FindObjectsOfType<T>().Length > 1)
                        {
                            return _instance;
                        }

                        if (_instance == null)
                        {
                            var singleton = new GameObject();
                            singleton.transform.position = Vector3.zero;
                            _instance = singleton.AddComponent<T>();
                            singleton.name = typeof(T).ToString() + "_" + "Singleton";
                            DontDestroyOnLoad(singleton);
                        }
                        else
                        {
                            Debug.LogWarning("Trying to create one more instance of object " +_instance.gameObject.name);
                        }
                    }

                    return _instance;
                }
            }
        }

        private static bool _isAlive;
        

        private void OnDestroy()
        {
            _isAlive = true;
        }

        private void OnApplicationQuit()
        {
            _isAlive = true;
        }
        
    }
}