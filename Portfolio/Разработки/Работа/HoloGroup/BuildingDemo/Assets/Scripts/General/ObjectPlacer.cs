﻿namespace Assets.Scripts.General
{
    using HoloToolkit.Unity;
    using UnityEngine;

    public class ObjectPlacer : MonoBehaviour
    {
        private readonly float _distanceThreshold = 0.02f;

        private readonly float _hoverDistance = 0.15f;

        private readonly float _maximumPlacementDistance = 5.0f;

        private readonly float _placementVelocity = 0.06f;

        private Vector3 _targetPosition;

        private bool _isPlacing;

        private BoxCollider _collider;

        public bool IsPlacing
        {
            get { return _isPlacing; }
            set
            {
                _isPlacing = value;
                if(_isPlacing)
                    OnPlacementStart();
                else 
                    OnPlacementStop();
            }
        }

        private void Awake()
        {
            _targetPosition = gameObject.transform.position;
            _collider = GetComponent<BoxCollider>();
        }

        private void Update()
        {
            if (IsPlacing)
            {
                Move();
            }
            else
            {
                var dist = (gameObject.transform.position - _targetPosition).magnitude;
                if (dist > 0)
                    gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, _targetPosition,
                        _placementVelocity/dist);
            }
        }

        private bool ValidatePlacement(out Vector3 position, out Vector3 surfaceNormal)
        {
            var raycastDirection = -Vector3.up;
            position = Vector3.zero;
            surfaceNormal = Vector3.zero;

            var facePoints = GetColliderFacePoints();

            for (var i = 0; i < facePoints.Length; i++)
                facePoints[i] = gameObject.transform.TransformVector(facePoints[i]) + gameObject.transform.position;

            RaycastHit centerHit;
            if (!Physics.Raycast(facePoints[0],
                raycastDirection,
                out centerHit,
                _maximumPlacementDistance,
                SpatialMappingManager.Instance.LayerMask))
                return false;

            position = centerHit.point;
            surfaceNormal = centerHit.normal;

            for (var i = 1; i < facePoints.Length; i++)
            {
                RaycastHit hitInfo;
                if (Physics.Raycast(facePoints[i],
                    raycastDirection,
                    out hitInfo,
                    _maximumPlacementDistance,
                    SpatialMappingManager.Instance.LayerMask))
                {
                    if (!IsEquivalentDistance(centerHit.distance, hitInfo.distance))
                        return false;
                }
                else
                    return false;
            }

            return true;
        }

        private Vector3[] GetColliderFacePoints()
        {
            var extents = _collider.size/2;

            var minX = _collider.center.x - extents.x;
            var maxX = _collider.center.x + extents.x;
            var minY = _collider.center.y - extents.y;
            var minZ = _collider.center.z - extents.z;
            var maxZ = _collider.center.z + extents.z;

            var center = new Vector3(_collider.center.x, minY, _collider.center.z);
            var corner0 = new Vector3(minX, minY, minZ);
            var corner1 = new Vector3(minX, minY, maxZ);
            var corner2 = new Vector3(maxX, minY, minZ);
            var corner3 = new Vector3(maxX, minY, maxZ);

            return new[] {center, corner0, corner1, corner2, corner3};
        }

        private void OnPlacementStart()
        {
            _collider.enabled = true;
        }

        private void OnPlacementStop()
        {
            Vector3 position;
            Vector3 surfaceNormal;

            if (!ValidatePlacement(out position, out surfaceNormal))
                return;

            _targetPosition = position + 0.1f*surfaceNormal;
            _collider.enabled = false;
            gameObject.transform.position = _targetPosition;
            IsPlacing = false;
        }

        private void Move()
        {
            Vector3 moveTo;
            RaycastHit hitInfo;

            var hit = Physics.Raycast(Camera.main.transform.position,
                Camera.main.transform.forward,
                out hitInfo,
                20f,
                SpatialMappingManager.Instance.LayerMask);

            if (hit)
            {
                var offsetDistance = _hoverDistance;

                if (hitInfo.distance <= _hoverDistance)
                    offsetDistance = 0f;

                moveTo = hitInfo.point + offsetDistance * hitInfo.normal;
            }
            else
                return;
            ;

            var dist = Mathf.Abs((gameObject.transform.position - moveTo).magnitude);
            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, moveTo, _placementVelocity/dist);
            OrientObject();
        }

        private void OrientObject()
        {
            gameObject.transform.rotation = Quaternion.Euler(0,Camera.main.transform.rotation.eulerAngles.y,
                Camera.main.transform.rotation.eulerAngles.z);
        }

        private bool IsEquivalentDistance(float d1, float d2)
        {
            var dist = Mathf.Abs(d1 - d2);
            return dist <= _distanceThreshold;
        }
    }
}