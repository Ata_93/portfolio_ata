﻿namespace Assets.Scripts.General
{
    using UnityEngine;

    public interface ITransformable
    {
        void OnStart(Vector3 delta);

        void OnUpdate(Vector3 delta);

        void OnComplete(Vector3 delta);

    }
}