﻿namespace Assets.Scripts.General
{
    using UnityEngine;

    /// <summary>
    ///     Script similar to TagAlong, except it allows object to follow camera and keep limited position by Y.
    /// </summary>
    public class ObjectRelativeTransform : MonoBehaviour
    {
        [Tooltip("Distance from camera")]
        [SerializeField]
        private float _direction;

        [Tooltip("Is folowing Camera")]
        [SerializeField]
        private bool _isFolowingCamera;

        [Tooltip("Y angle limit")]
        [Range(0, 60)]
        [SerializeField]
        private float _yRotationLimit;

        private void Update()
        {
            if (Camera.main == null)
                return;
            var directionToTarget = Camera.main.transform.position - gameObject.transform.position;

            if (directionToTarget.sqrMagnitude < 0.001f)
                return;

            directionToTarget.y = 0.0f;
            if ((Camera.main.transform.rotation.eulerAngles.x > _yRotationLimit) || (Camera.main.transform.rotation.eulerAngles.x < -_yRotationLimit))
                return;
            //gameObject.transform.rotation = Camera.main.transform.rotation;
            gameObject.transform.rotation = Quaternion.LookRotation(-directionToTarget);
            if (_isFolowingCamera)
                transform.position = Camera.main.transform.position + Camera.main.transform.forward*_direction;
        }
    }
}