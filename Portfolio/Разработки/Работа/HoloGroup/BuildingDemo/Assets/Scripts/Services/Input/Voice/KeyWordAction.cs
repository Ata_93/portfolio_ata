﻿using System;

using UnityEngine.Events;

namespace Assets.Scripts.Services.Input.Voice
{
    [Serializable]
    public struct KeyWordAction
    {
        public string KeyWord;

        public UnityEvent Action;
        
    }
}