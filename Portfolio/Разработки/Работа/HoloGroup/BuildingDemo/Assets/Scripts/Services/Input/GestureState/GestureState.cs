﻿using UnityEngine;

namespace Assets.Scripts
{
    public abstract class GestureState
    {
        public virtual void OnStart(GameObject focusedObject, Vector3 delta)
        {
            Debug.Log("Start!");
        }

        public virtual void OnUpdate(GameObject focusedObject, Vector3 delta)
        {
            Debug.Log("Update");
        }

        public virtual void OnCancel(GameObject focusedObject, Vector3 delta)
        {
            Debug.Log("Cancel");
        }

        public virtual void OnComplete(GameObject focusedObject, Vector3 delta)
        {
            Debug.Log("Complete");
        }
    }
}