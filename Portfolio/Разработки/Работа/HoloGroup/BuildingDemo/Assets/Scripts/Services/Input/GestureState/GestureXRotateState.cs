﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GestureXRotateState: GestureState
    {
        private Vector3 _worldCameraRight;

        private float _horizontalSpeed;

        public GestureXRotateState(float velocity)
        {
            _horizontalSpeed = velocity;
        }

        public override void OnStart(GameObject focusedObject, Vector3 delta)
        {
            _worldCameraRight = Camera.main.transform.TransformDirection(Vector3.right);
        }

        public override void OnUpdate(GameObject focusedObject, Vector3 delta)
        {
            focusedObject.transform.Rotate(_worldCameraRight, delta.y * _horizontalSpeed, Space.World);
        }

        public override void OnComplete(GameObject focusedObject, Vector3 delta)
        {
            focusedObject.transform.Rotate(_worldCameraRight, delta.y * _horizontalSpeed, Space.World);
        }
    }
}