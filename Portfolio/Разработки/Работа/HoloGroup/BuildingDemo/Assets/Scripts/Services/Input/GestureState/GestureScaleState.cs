﻿using Assets.Scripts.General.Library;
using UnityEngine;

namespace Assets.Scripts
{
    public class GestureScaleState : GestureState
    {
        private float _minScale = 0.001f;

        private float _maxScale = 0.075f;

        private float _minSize;

        private float _maxSize;

        private Vector3 _initialScale;

        private float _scaleSens;

        public GestureScaleState(float scaleSensitivity,float minSize, float maxSize)
        {
            _scaleSens = scaleSensitivity;
            _minSize = minSize;
            _maxSize = maxSize;
        }

        private void CalculateMinMaxScale(GameObject focusedObject)
        {
            var bounds = focusedObject.GetBounds();
            var maxBound = Mathf.Max(bounds.size.x, bounds.size.y, bounds.size.z);
            _maxScale = _maxSize / maxBound;
            _minScale = _minSize / maxBound;
        }

        public override void OnStart(GameObject focusedObject, Vector3 delta)
        {
            if (_initialScale == Vector3.zero)
                _initialScale = focusedObject.transform.localScale;
            CalculateMinMaxScale(focusedObject);
            Debug.Log(focusedObject.transform.localScale);
        }
        
        public override void OnUpdate(GameObject focusedObject, Vector3 delta)
        {
            var scaleSensFloat = (_maxScale - _minScale);
            focusedObject.transform.localScale = Vector3.one*
                                                 Mathf.Clamp(_initialScale.y + delta.y*scaleSensFloat*_scaleSens,
                                                     _minScale, _maxScale);
            
        }
    }
}