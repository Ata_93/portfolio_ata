﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GestureYRotateState : GestureState
    {
        private Vector3 _worldCameraUp;

        private float _verticalSpeed;

        public GestureYRotateState(float velocity)
        {
            _verticalSpeed = velocity;
        }

        public override void OnStart(GameObject focusedObject, Vector3 delta)
        {
            _worldCameraUp = Camera.main.transform.TransformDirection(Vector3.up);
        }

        public override void OnUpdate(GameObject focusedObject, Vector3 delta)
        {
            focusedObject.transform.Rotate(-_worldCameraUp, delta.x * _verticalSpeed, Space.World);
            Debug.Log("Rotate X!");
        }

        public override void OnComplete(GameObject focusedObject, Vector3 delta)
        {
            focusedObject.transform.Rotate(-_worldCameraUp, delta.x * _verticalSpeed, Space.World);
            Debug.Log("Rotate X!");
        }
    }
}