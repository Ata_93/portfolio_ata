﻿using System;
using Assets.Scripts.General;
using HoloToolkit.Unity;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.VR.WSA.Input;

namespace Assets.Scripts.Services.Input
{
    public class CustomGestureManager : Custom.Singleton<CustomGestureManager>
    {
        private GestureRecognizer _recognizer;

        private ITransformable _currentTransformable;

        [SerializeField] private UnityEvent _onTapEvent;

        [SerializeField] private UnityEvent _onManipulationStart;
        [SerializeField] private UnityEvent _onManipulationUpdate;
        [SerializeField] private UnityEvent _onManipulationComplete;

        public event EventHandler SingleTapBehaviour;

        public void Init()
        {
            _recognizer = new GestureRecognizer();
            _recognizer.SetRecognizableGestures(GestureSettings.Tap | GestureSettings.ManipulationTranslate | GestureSettings.Hold);
            _recognizer.TappedEvent += RecognizerOnTappedEvent;
            _recognizer.ManipulationStartedEvent += RecognizerOnManipulationStartedEvent;
            _recognizer.ManipulationUpdatedEvent += _recognizer_ManipulationUpdatedEvent;
            _recognizer.ManipulationCompletedEvent += RecognizerOnManipulationCompletedEvent;
            _recognizer.ManipulationCanceledEvent += RecognizerOnManipulationCompletedEvent;
        }
        
        private void RecognizerOnManipulationStartedEvent(InteractionSourceKind source, Vector3 cumulativeDelta, Ray headRay)
        {
            _currentTransformable = GazeManager.Instance.FocusedObject.GetComponent<ITransformable>();
            if (_currentTransformable == null)
                return;
            _currentTransformable.OnStart(cumulativeDelta);
            if(_onManipulationStart != null)
                _onManipulationStart.Invoke();
        }

        private void _recognizer_ManipulationUpdatedEvent(InteractionSourceKind source, Vector3 cumulativeDelta, Ray headRay)
        {
            if (_currentTransformable == null)
                return;
            _currentTransformable.OnUpdate(cumulativeDelta);
            if(_onManipulationUpdate != null)
                _onManipulationUpdate.Invoke();
        }

        private void RecognizerOnManipulationCompletedEvent(InteractionSourceKind source, Vector3 cumulativeDelta, Ray headRay)
        {
            if (_currentTransformable == null)
                return;
            _currentTransformable.OnComplete(cumulativeDelta);
            _currentTransformable = null;
            if(_onManipulationComplete != null)
                _onManipulationComplete.Invoke();
        }
        
        private void RecognizerOnTappedEvent(InteractionSourceKind source, int tapCount, Ray headRay)
        {
            if (SingleTapBehaviour != null)
            {
                SingleTapBehaviour.Invoke(this,EventArgs.Empty);
                return;
            }
            if (_onTapEvent != null)
                _onTapEvent.Invoke();
        }
    }
}