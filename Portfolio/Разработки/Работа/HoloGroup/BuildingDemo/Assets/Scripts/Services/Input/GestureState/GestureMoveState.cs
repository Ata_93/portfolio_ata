﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GestureMoveState : GestureState
    {
        private Vector3 _defaultPosition;

        private float _positionVelocity = 3f;

        public GestureMoveState(float velocity)
        {
            _positionVelocity = velocity;
        }

        public override void OnStart(GameObject focusedObject, Vector3 delta)
        {
            _defaultPosition = focusedObject.transform.position;
            focusedObject.transform.position = _defaultPosition + (delta * _positionVelocity);
        }

        public override void OnUpdate(GameObject focusedObject, Vector3 delta)
        {
            focusedObject.transform.position = _defaultPosition + (delta * _positionVelocity);
        }

        public override void OnComplete(GameObject focusedObject, Vector3 delta)
        {
            focusedObject.transform.position = _defaultPosition + (delta * _positionVelocity);
        }
    }
}