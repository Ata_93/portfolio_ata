﻿using System.Collections.Generic;
using Assets.Scripts.Custom;
using UnityEngine;

namespace Assets.Scripts
{
    public class GestureContext : Singleton<GestureContext>
    {
        private GestureState _currentState;

        private List<GestureState> _gestureStates;

        [SerializeField]
        [Range(1,10)]
        private float _moveVelocity;

        [SerializeField]
        [Range(5, 15)]
        private float _rotationVelocity;

        [SerializeField]
        [Range(5, 15)]
        private float _scaleSensitivity;

        [SerializeField]
        [Range(0.4f, 1)]
        private float _minScaleSize;

        [SerializeField]
        [Range(1, 2.8f)]
        private float _maxScaleSize;


        private void Awake()
        {
           _gestureStates = new List<GestureState>()
           {
               new GestureMoveState(_moveVelocity),
               new GestureXRotateState(_rotationVelocity),
               new GestureYRotateState(_rotationVelocity),
               new GestureScaleState(_scaleSensitivity,_minScaleSize,_maxScaleSize)
           };
            _currentState = _gestureStates[0];
        }

        public void OnStart(GameObject focusedObject, Vector3 delta)
        {
            _currentState.OnStart(focusedObject,delta);
        }

        public void OnUpdate(GameObject focusedObject, Vector3 delta)
        {
            _currentState.OnUpdate(focusedObject, delta);
        }

        public void OnComplete(GameObject focusedObject, Vector3 delta)
        {
            _currentState.OnComplete(focusedObject, delta);
        }

        public void SwitchState()
        {
            _gestureStates.GetEnumerator().MoveNext();
            _currentState = _gestureStates.GetEnumerator().Current;
        }
    }
}