﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.General;
using Assets.Scripts.Services.Input.Voice;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Windows.Speech;

namespace Assets.Scripts.ScenesManagers.UI
{
    public class CustomVoiceManager : MonoBehaviour
    {
        [SerializeField] protected List<KeyWordAction> KeyWordActions;

        protected KeywordRecognizer Recognizer;

        private Dictionary<string, UnityEvent> _invokableEvents;

        private void Awake()
        {
            _invokableEvents = KeyWordActions.ToDictionary(t => t.KeyWord, k => k.Action);
            Recognizer = new KeywordRecognizer(KeyWordActions.Select(t => t.KeyWord).ToArray());
            Recognizer.OnPhraseRecognized += Recognizer_OnPhraseRecognized;
            Recognizer.Start();
        }

        private void Recognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
        {
            var words = args.text;
            if (!_invokableEvents.ContainsKey(words))
                return;
            if (args.confidence != ConfidenceLevel.High)
            {
                HintAndError.ShowError("Couldn't recognize the phrase. Please try again");
                return;
            }
            _invokableEvents[args.text].Invoke();
        }
        
        private void OnDestroy()
        {
            if (Recognizer != null)
            {
                Recognizer.Stop();
                Recognizer.Dispose();
            }
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Z))
                _invokableEvents["Show Roof"].Invoke();
            if(Input.GetKeyDown(KeyCode.X))
                _invokableEvents["Hide Roof"].Invoke();
            if(Input.GetKeyDown(KeyCode.C))
                _invokableEvents["Show Furniture"].Invoke();
            if(Input.GetKeyDown(KeyCode.V))
                _invokableEvents["Hide Furniture"].Invoke();
            if(Input.GetKeyDown(KeyCode.B))
                _invokableEvents["Inside"].Invoke();
            if (Input.GetKeyDown(KeyCode.N))
                _invokableEvents["Outside"].Invoke();
            if (Input.GetKeyDown(KeyCode.G))
                _invokableEvents["Default"].Invoke();
            if (Input.GetKeyDown(KeyCode.F))
                _invokableEvents["Place"].Invoke();

        }
    }
}