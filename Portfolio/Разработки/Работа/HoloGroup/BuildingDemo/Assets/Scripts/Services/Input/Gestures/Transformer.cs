﻿using Assets.Scripts.General;
using UnityEngine;
using UnityEngine.VR.WSA.Input;

namespace Assets.Scripts
{
    public class Transformer : MonoBehaviour, ITransformable
    {
        public void OnStart(Vector3 delta)
        {
            GestureContext.Instance.OnStart(gameObject, delta);
        }

        public void OnUpdate(Vector3 delta)
        {
            GestureContext.Instance.OnUpdate(gameObject, delta);
        }

        public void OnComplete(Vector3 delta)
        {
            GestureContext.Instance.OnComplete(gameObject, delta);
        }
    }
}