// Unlit shader. Simplest possible textured shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "Unlit/TextureFadeInTransparent" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_Alpha ("Alpha", Float) = 1
}

SubShader {
	Tags { "RenderType"="Transparent" "Queue" = "Transparent-6" }
	LOD 100

	Pass {
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite On
		ZTest LEqual
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata_t {
				half4 vertex : POSITION;
				half2 texcoord : TEXCOORD0;
			};

			struct v2f {
				half4 vertex : SV_POSITION;
				half2 texcoord : TEXCOORD0;
				UNITY_FOG_COORDS(1)
			};
			
			fixed _Alpha;
			sampler2D _MainTex;
			half4 _MainTex_ST;
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.texcoord);
				UNITY_APPLY_FOG(i.fogCoord, col);
				col.a = _Alpha;
				return col;
			}
		ENDCG
	}
}

}
